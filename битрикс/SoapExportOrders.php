<?

namespace Local\SoapExport;

use Bitrix\Main\Application;
use Bitrix\Sale;
use Exception;
use soapImport;
use CSaleOrder;

$root = Application::getDocumentRoot() . Application::getPersonalRoot();
include_once($root . '/php_interface/include/Logger.php');
include_once($root . '/php_interface/soap_import.php');

class SoapExportOrders extends soapImport {
    private $orderId;
    private $arClients = [];
    private $idCitySPB = 23133;
    private $idCityMSK = 23132;
    private $curCity;
    //private $timeSelectionOrders = 86400;
    private $timeSelectionOrders = 3600;
    private $paymentTypeMatchings = [
        1 => 1, // "Наличные курьеру" на сайте приравниваем к "Наличные" в API
        '' => 2, // "Карта" на сайте приравниваем к "Карта" в API
        6 => 3, // "Банковский перевод" на сайте приравниваем к "Безналичный расчет" в API
    ];
    private $deliveryTypeMatchings = [
        2 => 0, // "Самовывоз" на сайте приравниваем к "Самовывоз" в API
        20 => 0, // "Без доставки" на сайте приравниваем к "Самовывоз" в API
        1 => 1, // "Грузобагаж" на сайте приравниваем к "Заказ доставки" в API
    ];
    private $paymentTypeAPI;
    private $deliveryTypeAPI;
    private $entityAPIID = '006a0b80-baf7-433e-bc85-f622418f13b1';
    private $individualAPIID = 'b3a33e3e-0191-4338-9629-f4872548ddf7';

    private function init($exportAllOrders = true)
    {
        $this->curCity = (isset($_COOKIE["BITRIX_SM_mycity"]) && in_array($_COOKIE["BITRIX_SM_mycity"], array($this->idCitySPB, $this->idCityMSK))) ? $_COOKIE["BITRIX_SM_mycity"] : $this->idCityMSK;
        if($_GET['city-change'] == $this->idCitySPB OR $_GET['city-change'] == $this->idCityMSK)
            $this->curCity = $_GET['city-change'];

        $this->arClients = $this->_client->GetDpfClients()->GetDpfClientsResult->DpfClient;

        if(!empty($this->arClients)) {
            if($exportAllOrders) {
                $this->log('Получен список клиентов от API.', false);
                $this->logger->addRow([-1, 'Получен список клиентов от API.',]);
            } else {
                $this->log('Заказ(' . $this->orderId . '). Получен список клиентов от API.', false);
                $this->logger->addRow([$this->orderId, 'Получен список клиентов от API.',]);
            }
        } else {
            if($exportAllOrders) {
                $this->log('Не удалось получить список клиентов от API (СРОЧНО проверьте работу API).', false);
                $this->logger->addRow([-1, 'Не удалось получить список клиентов от API (СРОЧНО проверьте работу API).',]);
                $this->error_log($this->_import_id . ': Ошибка. Не удалось получить список клиентов от API (СРОЧНО проверьте работу API).', false);
            } else {
                $this->log('Заказ(' . $this->orderId . '). Не удалось получить список клиентов от API (СРОЧНО проверьте работу API).', false);
                $this->logger->addRow([$this->orderId, 'Не удалось получить список клиентов от API (СРОЧНО проверьте работу API).',]);
                $this->error_log($this->_import_id . ': Ошибка. Заказ(' . $this->orderId . '). Не удалось получить список клиентов от API (СРОЧНО проверьте работу API).', false);
            }

            return false;
        }

        $this->arTerritories = $this->_client->GetDpfTerritories()->GetDpfTerritoriesResult->DpfTerritory;

        if(!empty($this->arTerritories)) {
            if($exportAllOrders) {
                $this->log('Получен список территорий от API.', false);
                $this->logger->addRow([-1, 'Получен список территорий от API.',]);
            } else {
                $this->log('Заказ(' . $this->orderId . '). Получен список территорий от API.', false);
                $this->logger->addRow([$this->orderId, 'Получен список территорий от API.',]);
            }
        } else {
            if($exportAllOrders) {
                $this->log('Не удалось получить список территорий от API (СРОЧНО проверьте работу API).', false);
                $this->logger->addRow([-1, 'Не удалось получить список территорий от API (СРОЧНО проверьте работу API).',]);
                $this->error_log($this->_import_id . ': Ошибка. Не удалось получить список территорий от API (СРОЧНО проверьте работу API).', false);
            } else {
                $this->log('Заказ(' . $this->orderId . '). Не удалось получить список территорий от API (СРОЧНО проверьте работу API).', false);
                $this->logger->addRow([$this->orderId, 'Не удалось получить список территорий от API (СРОЧНО проверьте работу API).',]);
                $this->error_log($this->_import_id . ': Ошибка. Заказ(' . $this->orderId . '). Не удалось получить список территорий от API (СРОЧНО проверьте работу API).', false);
            }

            return false;
        }

        return true;
    }

    public function exportOrder($orderId) {
        $this->_import_id = 'exportOrder';
        $this->logger->setHeader(['ID заказа', ' ', 'Результат']);
        $this->log('Заказ(' . $orderId . '). Начало экспорта.', false);
        $this->logger->addRow([$orderId, 'Начало экспорта.',]);

        try {
            $this->orderId = $orderId;

            if(!$this->init(false)) {
                $this->sendEmails($this->_import_id);

                return false;
            }

            if($this->exportOneOrder()) {
                $this->log('Заказ(' . $this->orderId . '). Заказ успешно отправлен.', false);
                $this->logger->setFooter('Заказ(' . $this->orderId . ') успешно отправлен.');

                $this->sendEmails($this->_import_id);
            } else {
                $this->sendEmails($this->_import_id);

                return false;
            }
        } catch (Exception $e) {
            $this->log('Заказ(' . $this->orderId . '). Произошла неизвестная ошибка при экспорте: ' . print_r($e->getMessage(), 1), false);
            $this->logger->addRow([$this->orderId, 'Произошла неизвестная ошибка при экспорте: ' . print_r($e->getMessage(), 1),]);
            $this->error_log('exportOrder: Заказ(' . $this->orderId . '). Произошла неизвестная ошибка при экспорте: ' . print_r($e->getMessage(), 1), false);

            $this->sendEmails($this->_import_id);

            return false;
        }

        return true;
    }

    public function getUserIdAPI($userIdWeb) {
        foreach ($this->arClients as $client) {
            foreach ($client->WebSiteIDs as $value) {
                if(is_array($value)) {
                    if(in_array($userIdWeb, $value)) {
                        return $client->ID;
                    }
                } else {
                    if($value == $userIdWeb) {
                        return $client->ID;
                    }
                }
            }
        }

        return false;
    }

    public function logUserVisitedConfirmPage($orderId) {
        $this->_import_id = 'exportOrder';
        $this->log('Заказ(' . $orderId . '). Заказ на сайте оформлен и клиент попал на страницу confirm.php.', false);
    }

    public function exportOneOrder() {
        $order = Sale\Order::load($this->orderId);

        if(empty($order)) {
            $this->log('Ошибка: Не удалось найти заказ №' . $this->orderId);
            $this->error_log($this->_import_id . ': Не удалось найти заказ №' . $this->orderId);
            $this->logger->addRow([$this->orderId, ' ', 'Ошибка: Не удалось найти заказ №' . $this->orderId]);

            return false;
        }

        $itemsInOrder = [];
        $globalPlaceID = '';
        $basket = Sale\Order::load($this->orderId)->getBasket();
        $basketItems = $basket->getBasketItems();

        if(!empty($basketItems)) {
            $this->log('Заказ(' . $this->orderId . '). Получена корзина для заказа.', false);
            $this->logger->addRow([$this->orderId, 'Получена корзина для заказа.',]);
        } else {
            $this->log('Ошибка: Не удалось получить корзину для заказа №' . $this->orderId);
            $this->error_log($this->_import_id . ': Не удалось получить корзину для заказа №' . $this->orderId);
            $this->logger->addRow([$this->orderId, ' ', 'Ошибка: Не удалось получить корзину для заказа №' . $this->orderId]);

            return false;
        }

        foreach ($basketItems as $basketItem) {
            $itemsInOrder[] = array(
                'CargoID' => $basketItem->getField('PRODUCT_XML_ID'),
                'Quantity' => $basketItem->getQuantity(),
            );
        }

        $userIdWeb = $order->getUserId();

        if($userIdWeb === false) {
            $this->log('Заказ(' . $this->orderId . '). Не удалось получить ID пользователя из заказа', false);
            $this->logger->addRow([$this->orderId, 'Не удалось получить ID пользователя из заказа',]);
            $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Не удалось получить ID пользователя из заказа', false);

            return false;
        }

        $rsUser = \CUser::GetByID($userIdWeb);
        $arUser = $rsUser->Fetch();

        if(!$arUser) {
            $this->log('Заказ(' . $this->orderId . '). Пользователь с таким ID не существует.', false);
            $this->logger->addRow([$this->orderId, 'Пользователь с таким ID не существует.',]);
            $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Пользователь с таким ID не существует.', false);

            return false;
        }

        $userIdAPI = $this->getUserIdAPI($userIdWeb);

        $propertyCollection = $order->getPropertyCollection();

        if($userIdAPI === false) {
            $userType = $order->getPersonTypeId();

            if($userType == 1) {
                $userIdAPI = $this->individualAPIID;
            } elseif ($userType == 2) {
                $userIdAPI = $this->entityAPIID;
            } else {
                $this->log('Заказ(' . $this->orderId . '). Не удалось получить API ID пользователя для заказа', false);
                $this->logger->addRow([$this->orderId, 'Не удалось получить API ID пользователя для заказа',]);
                $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Не удалось получить API ID пользователя для заказа', false);

                return false;
            }
        }

        $this->log('Заказ(' . $this->orderId . '). Получено API ID пользователя для заказа.', false);
        $this->logger->addRow([$this->orderId, 'Получено API ID пользователя для заказа.',]);

        //берем город с заказа
        $locPropValue = $propertyCollection->getDeliveryLocation();
        $locId = $locPropValue->getValue();
        if ($locId) {
            $arLoc = \CSaleLocation::GetByID($locId);
            $cityNameOrder = $arLoc['CITY_NAME'];
            foreach ($this->arTerritories as $arTerritory) {
                if ($cityNameOrder == $arTerritory->Name) {
                    $globalPlaceID = $arTerritory->ID;
                    break;
                }
            }
        }

        /* старый код получения города
        foreach ($this->arTerritories as $arTerritory) {
            if(($this->curCity == $this->idCitySPB)
                && ($arTerritory->Name == 'Санкт-Петербург')) {
                $globalPlaceID = $arTerritory->ID;
            } elseif(($this->curCity == $this->idCityMSK)
                && ($arTerritory->Name == 'Москва')) {
                $globalPlaceID = $arTerritory->ID;
            }
        }
        */

        if(!empty($globalPlaceID)) {
            $this->log('Заказ(' . $this->orderId . '). Получен город для заказа.', false);
            $this->logger->addRow([$this->orderId, 'Получен город для заказа.',]);
        } else {
            $this->log('Заказ(' . $this->orderId . '). Не удалось получить город для заказа.', false);
            $this->logger->addRow([$this->orderId, 'Не удалось получить город для заказа.',]);
            $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Не удалось получить город для заказа.', false);

            return false;
        }

        $comment = $order->getField('USER_DESCRIPTION');

        $paymentIds = $order->getPaymentSystemId();

        if(!empty($paymentIds)){
            $firstPaySystem = reset($paymentIds);

            if(isset($this->paymentTypeMatchings[$firstPaySystem])) {
                $this->paymentTypeAPI = $this->paymentTypeMatchings[$firstPaySystem];

                $this->log('Заказ(' . $this->orderId . '). Получен способ оплаты.', false);
                $this->logger->addRow([$this->orderId, 'Получен способ оплаты.',]);
            } else {
                $this->log('Заказ(' . $this->orderId . '). Не удалось получить способ оплаты.', false);
                $this->logger->addRow([$this->orderId, 'Не удалось получить способ оплаты.',]);
                $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Не удалось получить способ оплаты.', false);

                return false;
            }
        } else {
            $this->log('Заказ(' . $this->orderId . '). Не удалось получить способ оплаты.', false);
            $this->logger->addRow([$this->orderId, 'Не удалось получить способ оплаты.',]);
            $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Не удалось получить способ оплаты.', false);

            return false;
        }

        if(!empty($order->getField('DELIVERY_ID'))) {
            if(isset($this->deliveryTypeMatchings[$order->getField('DELIVERY_ID')])) {
                $this->deliveryTypeAPI = $this->deliveryTypeMatchings[$order->getField('DELIVERY_ID')];

                $this->log('Заказ(' . $this->orderId . '). Получен способ доставки.', false);
                $this->logger->addRow([$this->orderId, 'Получен способ доставки.',]);
            } else {
                $this->log('Заказ(' . $this->orderId . '). Не удалось получить способ доставки.', false);
                $this->logger->addRow([$this->orderId, 'Не удалось получить способ доставки.',]);
                $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Не удалось получить способ доставки.', false);

                return false;
            }
        } else {
            $this->log('Заказ(' . $this->orderId . '). Не удалось получить способ доставки.', false);
            $this->logger->addRow([$this->orderId, 'Не удалось получить способ доставки.',]);
            $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Не удалось получить способ доставки.', false);

            return false;
        }

        $emailPropValue = $propertyCollection->getUserEmail();
        $phonePropValue = $propertyCollection->getPhone();

        $email = $emailPropValue->getValue();

        if(!empty($phonePropValue)) {
            $phone = $phonePropValue->getValue();
        } else {
            $phone = isset($arUser['PERSONAL_PHONE']) ? $arUser['PERSONAL_PHONE'] : '';
        }

        if($order->getField('DELIVERY_ID') == 1) {
            $streetDeliveryVal = '';
            $houseDeliveryVal = '';
            $korpusDeliveryVal = '';
            $numberDeliveryVal = '';

            foreach ($propertyCollection as $prop) {
                if($prop->getField('CODE') == 'STREET') {
                    $streetDeliveryVal = $prop->getValue();
                }
                if($prop->getField('CODE') == 'HOUSE') {
                    $houseDeliveryVal = $prop->getValue();
                }
                if($prop->getField('CODE') == 'KORPUS') {
                    $korpusDeliveryVal = $prop->getValue();
                }
                if($prop->getField('CODE') == 'NUMBER') {
                    $numberDeliveryVal = $prop->getValue();
                }
            }

            if(!empty($streetDeliveryVal) or !empty($houseDeliveryVal)
                or !empty($korpusDeliveryVal) or !empty($numberDeliveryVal)) {
                if(!empty($comment)) {
                    $comment .= '\r\n';
                    $comment .= '\r\n';
                }

                $comment .= 'Адрес доставки:\r\n';

                $comment .= 'Улица:\r\n';

                if(!empty($streetDeliveryVal)) {
                    $comment .= $streetDeliveryVal . '\r\n';
                } else {
                    $comment .= 'Нет данных' . '\r\n';
                }

                $comment .= 'Дом:\r\n';

                if(!empty($houseDeliveryVal)) {
                    $comment .= $houseDeliveryVal . '\r\n';
                } else {
                    $comment .= 'Нет данных' . '\r\n';
                }

                $comment .= 'Корпус:\r\n';

                if(!empty($korpusDeliveryVal)) {
                    $comment .= $korpusDeliveryVal . '\r\n';
                } else {
                    $comment .= 'Нет данных' . '\r\n';
                }

                $comment .= 'Кв/офис:\r\n';

                if(!empty($numberDeliveryVal)) {
                    $comment .= $numberDeliveryVal . '\r\n';
                } else {
                    $comment .= 'Нет данных' . '\r\n';
                }
            }
        }

        if($userType == 2) {
            foreach ($propertyCollection as $prop) {
                if ($prop->getField('CODE') == 'INN') {
                    $taxID = $prop->getValue();
                }
            }
        }

        if(isset($taxID) && !empty($taxID)) {
            if (preg_match('/[^0-9]/', $taxID)) {
                $taxID = '';
            }

            if ((strlen($taxID) < 10) || (strlen($taxID) > 12)) {
                $taxID = '';
            }
        } else {
            $taxID = '';
        }

        $dataExport = array(
            'SiteWebOrderID' => $this->orderId,
            'Date' => strtotime($order->getDateInsert()->toString()),
            'ClientID' => $userIdAPI,
            'GlobalPlaceID' => $globalPlaceID,
            'Comment' => $comment,
            'Items' => $itemsInOrder,
            'PaymentType' => $this->paymentTypeAPI,
            'DeliveryType' => $this->deliveryTypeAPI,
            'ClientEmail' => $email,
            'ClientPhone' => $phone,
            'TaxId' => $taxID,
        );

        $result = $this->_client->CreateDpfWebOrder(array('newWebOrder' => $dataExport));

        if(isset($result->CreateDpfWebOrderResult)
            && ($result->CreateDpfWebOrderResult == 1)) {
            foreach ($propertyCollection as $prop) {
                if($prop->getField('CODE') == 'SENT_TO_API') {
                    $res = $prop->setField('VALUE', 1);

                    $order->save();
                }
            }

            return true;
        } else {
            $this->log('Заказ(' . $this->orderId . '). Не удалось отправить заказ в API. Отправляемые данные: ' . print_r($dataExport, 1), false);
            $this->logger->addRow([$this->orderId, 'Не удалось отправить заказ в API. Отправляемые данные: ' . print_r($dataExport, 1),]);
            $this->error_log('exportOrder: Ошибка. Заказ(' . $this->orderId . '). Не удалось отправить заказ в API. Отправляемые данные: ' . print_r($dataExport, 1), false);

            return false;
        }
    }

    public function exportAllOrders() {
        $this->_import_id = 'exportAllOrders';
        $this->log('Экспорт заказов начат.', false);
        $this->logger->setHeader(['ID заказа', ' ', 'Результат']);

        try {
            if(!$this->init()) {
                $this->sendEmails($this->_import_id);

                return false;
            }

            $listOrders = array();
            $neededTime = date('d.m.Y H:i:s', (time() - $this->timeSelectionOrders));
            $arFilter = array(
                '>DATE_INSERT' => $neededTime,
                '!PROPERTY_VAL_BY_CODE_SENT_TO_API' => 1,
            );

            $resOrders = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter, false, false, array('ID', 'DATE_INSERT', 'USER_ID', 'STATUS_ID', 'PROPERTY_VAL_BY_CODE_SENT_TO_API'));

            while ($arOrder = $resOrders->Fetch()) {
                $listOrders[] = $arOrder;
            }

            if(!empty($listOrders)) {
                foreach ($listOrders as $order) {
                    $this->log('Заказ(' . $order['ID'] . '). Начало экспорта.', false);
                    $this->logger->addRow([$order['ID'], 'Начало экспорта.',]);
                    $this->orderId = $order['ID'];

                    if($this->exportOneOrder()) {
                        $this->log('Заказ(' . $this->orderId . '). Заказ успешно отправлен.', false);
                        $this->logger->addRow([$this->orderId, 'Заказ успешно отправлен',]);
                    } else {
                        return false;
                    }
                }
            }
        } catch (Exception $e) {
            if (strripos($e->getMessage(), "уже создан") !== false) {
                $order = Sale\Order::load($this->orderId);
                $propertyCollection = $order->getPropertyCollection();

                foreach ($propertyCollection as $prop) {
                    if($prop->getField('CODE') == 'SENT_TO_API') {
                        $res = $prop->setField('VALUE', 1);

                        $order->save();
                        break;
                    }
                }
            }

            $this->log('Произошла неизвестная ошибка при экспорте: ' . print_r($e->getMessage(), 1), false);
            $this->logger->addRow([-1, 'Произошла неизвестная ошибка при экспорте: ' . print_r($e->getMessage(), 1),]);
            $this->error_log('exportOrder: Произошла неизвестная ошибка при экспорте: ' . print_r($e->getMessage(), 1), false);

            return false;
        }

        $this->log('Экспорт заказов завершен.', false);
        $this->logger->setFooter('Экспорт заказов завершен.');
        //$this->sendEmails($this->_import_id);

        return true;
    }
}