#!/usr/bin/php71
<?php

$documentRoot = str_replace('\\', '/', realpath(dirname(__FILE__) . '/../../'));
$_SERVER['DOCUMENT_ROOT'] = $documentRoot;

require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/phpspreadsheet/vendor/autoload.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/classes/Parse.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/phpspreadsheet/chunkReadFilter.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/classes/CBRAgent.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/vendor/autoload.php');

class ParserBioshop {
    public $priceName = 'СведенияОНоменклатуре.xlsx';
    public $iblockId = 25;
    public $idStore = 2;        // ID склада поставщика
    public $idRetailPrice = 2;          // ID типа цены для розничной цены
    public $idDealersPrice = 3;         // ID типа цены для дилерской цены
    public $idFlagInStockProducer = 93; // ID флага о наличии товара у поставщика
    public $typeFileWithProducts = 'Xlsx';
    public $chunkSize = 50;

    public $folderStatistics = '/parce/log/';
    public $statPathFoundProd;
    public $statPathNotFoundProd;
    public $pathErrorFile;
    public $mask = '*bioshop_*.*'; // маска, чтобы удалить старые файлы логов
    public $logsLifeTime = 7*24*3600; // срок жизни файлов логов (в секундах)

    public function startParser() {
        $parse = new Parse();
        $this->statPathFoundProd = $this->folderStatistics . 'found_bioshop_'. date("d_m_Y_H_i_s") . '.csv';
        $this->statPathNotFoundProd = $this->folderStatistics . '/not_found_bioshop_'. date("d_m_Y_H_i_s") . '.csv';
        $this->pathErrorFile = $this->folderStatistics . '/bioshop_errors_'. date("d_m_Y_H_i_s") . '.csv';

        $parse->removeOldFilesStat($this->folderStatistics, $this->mask, $this->logsLifeTime);

        $linkUploadPriceList = $_SERVER['DOCUMENT_ROOT'].'/upload/tmp/trade_price_list/';

        mkdir($linkUploadPriceList);

        $linkUploadPriceList .= 'price_list_' . time() .'.xlsx';

        if($this->downloadPriceList($linkUploadPriceList)) {
            $startParce = date("d.m.Y H:i:s");

            $cbr = new CBRAgent();

            if ($cbr->load()) {
                if($this->parsePriceList($linkUploadPriceList, $cbr)) {
                    $parse->removeProductFromProduction($this->idFlagInStockProducer, $startParce, $cbr, $this->pathErrorFile);
                }
            } else {
                Parse::saveErrorMessage($this->pathErrorFile, 'Не удалось получить курсы валют от ЦБ');
            }
        } else {
            Parse::saveErrorMessage($this->pathErrorFile, 'Не удалось скачать указанный прайс');
        }

        unlink($linkUploadPriceList);
    }

    /**
     * Функция скачивает указанный прайс-лист поставщика
     * @param string $linkUploadPriceList
     * @return boolean
     */
    public function downloadPriceList($linkUploadPriceList) {
        $ftp = ftp_ssl_connect($this->serverFTPPriceList, $this->portFTPPriceList);

        $login_result = ftp_login($ftp, $this->loginFTPPriceList, $this->passFTPPriceList);

        if(!ftp_pasv($ftp, true)) {
            return FALSE;
        }

        if (ftp_get($ftp, $linkUploadPriceList, $this->priceName, FTP_BINARY)) {
            return TRUE;
        } else {
            return FALSE;
        }

        ftp_close($ftp);
    }

    /**
     * Функция парсит скачанный прайс-лист поставщика и записывает товары из него в БД Трейда
     * @param string $linkPriceList
	 * @param object $cbr
     */
    public function parsePriceList($linkPriceList, $cbr) {
        $f1 = fopen($_SERVER['DOCUMENT_ROOT'] . $this->statPathFoundProd,'w');
        $f2 = fopen($_SERVER['DOCUMENT_ROOT'] . $this->statPathNotFoundProd,'w');

        fputcsv($f1, array_map(array('Parse', 'convert_utf_in_cp'), array('Артикул', 'Артикул (Старый)', 'Название', 'Производитель', 'Модель', 'Цена Дилерская', 'Цена Розничная', 'Валюта', 'Курсы валют', 'Количество', 'Статус')), ';', '"');
        fputcsv($f2, array_map(array('Parse', 'convert_utf_in_cp'), array('Артикул', 'Артикул (Старый)', 'Название', 'Производитель', 'Модель', 'Цена Дилерская', 'Цена Розничная', 'Валюта', 'Курсы валют', 'Количество', 'Статус')), ';', '"');

        fclose($f1);
        fclose($f2);

        try {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($this->typeFileWithProducts);
            $reader->setReadDataOnly(true);
            //$reader->setReadEmptyCells(false);

            $worksheetData = $reader->listWorksheetInfo($linkPriceList);
            $countRowsInSheet = isset($worksheetData[0]['totalRows']) ? $worksheetData[0]['totalRows'] : 0;
            $chunkSize = $this->chunkSize;

            $chunkFilter = new ChunkReadFilter();

            $reader->setReadFilter($chunkFilter);

            for ($startRow = 0; $startRow <= $countRowsInSheet; $startRow += $chunkSize) {
                $chunkFilter->setRows($startRow, $chunkSize);
                $spreadsheet = $reader->load($linkPriceList)->getActiveSheet();

                $pRange = 'A' . $startRow . ':' . $spreadsheet->getHighestColumn() . $spreadsheet->getHighestRow();

                $sheetData = $spreadsheet->rangeToArray($pRange, $nullValue = null, $calculateFormulas = true, $formatData = true, $returnCellRef = true);

                //$sheetData = $spreadsheet->toArray(null, true, true, true);

                if(!empty($sheetData)) {
                    foreach($sheetData as $key => $value) {
                        if($key > 2) {
                            $this->saveProductInTrade($value, $cbr);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Parse::saveErrorMessage($this->pathErrorFile, $e->getMessage());

            return FALSE;
        }

        return TRUE;
    }

    /**
     * Функция добавляет новые товары на Трейд, которые приходят от поставщика Bioshop. Или обновляет уже существующие.
     * @param array $dataProduct
	 * @param object $cbr
     */
    public function saveProductInTrade($dataProduct, $cbr) {
        $parse = new Parse();
        $finalDataProduct = array();

        $finalDataProduct['amount'] = (isset($dataProduct['Z']) && !empty($dataProduct['Z'])) ? trim($dataProduct['Z']) : 0;
        $finalDataProduct['article'] = (isset($dataProduct['D']) && !empty($dataProduct['D']))? trim($dataProduct['D']) : '';
        $finalDataProduct['old_article'] = (isset($dataProduct['G']) && !empty($dataProduct['G'])) ? trim($dataProduct['G']) : '';
        $finalDataProduct['name'] = (isset($dataProduct['F']) && !empty($dataProduct['F'])) ? trim($dataProduct['F']) : '';
        $finalDataProduct['vendor'] = (isset($dataProduct['K']) && !empty($dataProduct['K'])) ? trim($dataProduct['K']) : '';
        $finalDataProduct['model'] = (isset($dataProduct['L']) && !empty($dataProduct['L'])) ? trim($dataProduct['L']) : '';
        $finalDataProduct['currency'] = (isset($dataProduct['AD']) && !empty($dataProduct['AD'])) ? trim($dataProduct['AD']) : 'RUB';

        $finalDataProduct['price_dealers'] = (isset($dataProduct['AC']) && !empty($dataProduct['AC'])) ? trim($dataProduct['AC']) : 0;
        $finalDataProduct['price_retail'] = (isset($dataProduct['AF']) && !empty($dataProduct['AF'])) ? trim($dataProduct['AF']) : 0;

        $neededElement = array();
        $manufacturer = $finalDataProduct['vendor'];

        if(!empty($finalDataProduct['old_article'])) {
            $neededElement = $this->getTradeProdByArticleBioshop($finalDataProduct['old_article']);
        }

        if (empty($neededElement) && !empty($finalDataProduct['article'])) {
            $neededElement = $this->getTradeProdByArticleBioshop($finalDataProduct['article']);
        }

        $realNameManufacturer = $parse->getManufacturerByAnalog($manufacturer);

        if(!empty($realNameManufacturer)) {
            $manufacturer = $realNameManufacturer;
            $finalDataProduct['vendor'] = $manufacturer;
        }

        if(!empty($finalDataProduct['vendor'])
            && !empty($finalDataProduct['model'])) {
            $clearModel = $parse->ClearModelName($finalDataProduct['vendor'], $finalDataProduct['model']);
        }

        /*if(empty($neededElement)
            && !empty($clearModel)) {
            $neededElement = $parse->getTradeProdByClearModel($clearModel);
        }*/

        if (!empty($neededElement)) {
            $statisticParth = $_SERVER['DOCUMENT_ROOT'] . $this->statPathFoundProd;

            if($this->updateProdInTrade($neededElement, $finalDataProduct, $cbr)) {
                $this->statisticFoundProducts($statisticParth, $finalDataProduct, $cbr, 'Успешно найден и обновлен');
            } else {
                $this->statisticFoundProducts($statisticParth, $finalDataProduct, $cbr, 'Товар найден, но произошла ошибка при обновлении');
            }
        } else {
            $statisticParth = $_SERVER['DOCUMENT_ROOT'] . $this->statPathNotFoundProd;

            $this->statisticFoundProducts($statisticParth, $finalDataProduct, $cbr, 'Товар не найден');
        }
    }

    /**
     * Функция находит товар на Трейде по артикулу с Bioshop
     * @param string $articleBioshop
     * @return array
     */
    public function getTradeProdByArticleBioshop($articleBioshop) {
        $arOrder = array("SORT" => "ASC");
        $arFilter = array(
            "IBLOCK_ID"=>$this->iblockId,
            "PROPERTY_ARTICLE_BIOSHOP" => '%' . $articleBioshop . '%',
            'CHECK_PERMISSIONS' => 'N'
        );

        $arSelectFields = array(
            "ID",
            "ACTIVE",
            "NAME",
            "IBLOCK_SECTION_ID",
            "PROPERTY_MODEL_PARCE",
            "PROPERTY_BRAND",
            "PROPERTY_BRAND.NAME",
            "PROPERTY_USE_RRC",
            "PROPERTY_PROVIDER_FOR_COEFFICIENT",
            "PROPERTY_PRICE_FROM_PROVIDER",
            "PROPERTY_ARTICLE_BIOSHOP",
            "PROPERTY_DO_NOT_CHANGE_PRICE"
        );

        $rsElements = CIBlockElement::GetList($arOrder, $arFilter, FALSE, FALSE, $arSelectFields);

        while ($arElement = $rsElements->GetNext()) {
            if(trim($arElement['~PROPERTY_ARTICLE_BIOSHOP_VALUE']) == $articleBioshop) {
                return $arElement;
            }
        }

        return array();
    }

    /**
     * Функция обновляет товар на Трейде по данным от поставщика
     * @param array $tradeProd
     * @param array $dataProdBioshop
     * @param object $cbr
     */
    public function updateProdInTrade($tradeProd, $dataProdBioshop, $cbr) {
        $el = new CIBlockElement;
        $parse = new Parse();

        $tradeProdID = $tradeProd['ID'];

        $arLoadProductArray = Array(
            "MODIFIED_BY" => 1,
            //"ACTIVE" => "Y"
        );

        if(empty($dataProdBioshop['price_dealers']) && empty($dataProdBioshop['price_retail'])) {
            Parse::saveErrorMessage($this->pathErrorFile, 'Не удалось обновить данные товара, т.к. у товара указаны неправильные цены (товар ' . $tradeProd['ID'] . ').');
            return false;
        }

        if($el->Update($tradeProdID, $arLoadProductArray)) {
            $inStockProducer = array();
            $res = CIBlockElement::GetProperty($this->iblockId, $tradeProdID, 'ID', 'DESC', array('ID' => 1391));

            while ($ob = $res->GetNext()) {
                $inStockProducer[] = $ob['VALUE'];
            }

            if(!in_array($this->idFlagInStockProducer, $inStockProducer)) {
                $inStockProducer[] = $this->idFlagInStockProducer;
            }

            CIBlockElement::SetPropertyValuesEx($tradeProdID, false, array('BIOSHOP_DATE' => date("d.m.Y H:i:s"), '1391' => $inStockProducer));

            /*if(!empty($dataProdBioshop['article'])) {
                CIBlockElement::SetPropertyValuesEx($tradeProdID, false, array('ARTICLE_BIOSHOP' => $dataProdBioshop['article']));
            }*/

            if(!$this->setCountProductForCurProvider($tradeProdID, $dataProdBioshop)) {
                Parse::saveErrorMessage($this->pathErrorFile, 'Не удалось задать количество товара на складе для поставщика (товар ' . $tradeProd['ID'] . ').');
                return false;
            }

            if(!$this->setPriceProductForCurProvider($tradeProdID, $dataProdBioshop)) {
                Parse::saveErrorMessage($this->pathErrorFile, 'Не удалось записать цены для поставщика (товар ' . $tradeProd['ID'] . ').');
                return false;
            }

            CIBlockElement::SetPropertyValuesEx($tradeProdID, false, array('REMOVED_FROM_PRODUCTION' => 0, 'PRICE_ON_REQUEST' => 0));

            if(isset($tradeProd['PROPERTY_USE_RRC_VALUE'])
                && !empty($tradeProd['PROPERTY_USE_RRC_VALUE'])) {
                if(!$parse->setMainPriceAndMainCountProductWithRRC($tradeProd, $cbr)) {
                    Parse::saveErrorMessage($this->pathErrorFile, 'Не удалось записать главную цену и количество (товар ' . $tradeProd['ID'] . ').');
                    return false;
                }
            } else {
                if(!$parse->setMainPriceAndMainCountProductWithCoef($tradeProd, $cbr)) {
                    Parse::saveErrorMessage($this->pathErrorFile, 'Не удалось записать главную цену и количество (товар ' . $tradeProd['ID'] . ').');
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Функция убирает у товара флажок "В наличии поставщика BIOSHOP". Тем самым товар помечается как "Снят с производства".
     * @param string $startParce
     */
    public function removeProductFromProduction($startParce) {
        $parse = new Parse();

        $arSort = array('SORT' => 'ASC', 'ID' => 'DESC');
        $arFilter = array("IBLOCK_ID" => $this->iblockId, 'ACTIVE' => 'Y', 'DATE_MODIFY_TO' => $startParce, 'CHECK_PERMISSIONS' => 'N');
        $arSelect = array('ID', 'NAME');

        $rsElement = CIBlockElement::getList($arSort, $arFilter, false, false, $arSelect);

        while ($arElement = $rsElement->GetNext()) {
            /* Убираем флажок "BIOSHOP" у товара */

            $inStockProducer = array();

            $res = CIBlockElement::GetProperty($this->iblockId, $arElement['ID'], 'ID', 'DESC', array('ID' => 1391));

            while ($ob = $res->GetNext()) {
                $inStockProducer[] = $ob['VALUE'];
            }

            if (($key = array_search($this->idFlagInStockProducer, $inStockProducer)) !== false) {
                unset($inStockProducer[$key]);
            }

            if(!empty($inStockProducer)) {
                CIBlockElement::SetPropertyValuesEx($arElement['ID'], false, array('1391' => $inStockProducer));
            } else {
                CIBlockElement::SetPropertyValuesEx($arElement['ID'], false, array('1391' => false));
            }

            /* Убираем флажок "BIOSHOP" у товара (КОНЕЦ) */

            $pricesProduct = $parse->getPricesProduct($arElement['ID']);
            $dataStorageProvider = $parse->getDataStorageById($this->idStore);

            if(isset($dataStorageProvider['ID'])) {
                $dataArray = Array(
                    "PRODUCT_ID" => $arElement['ID'],
                    "STORE_ID" => $dataStorageProvider['ID'],
                    "AMOUNT" => '',
                );

                $dataStorageForProd = $parse->getDataStorageForProd($arElement['ID'], $this->idStore);

                if(!empty($dataStorageForProd)) {
                    CCatalogStoreProduct::Delete($dataStorageForProd['ID']);
                }
            }

            $resPricesProduct = array();

            foreach($pricesProduct as $key=>$value) {
                if(($key != $this->idDealersPrice)
                    && ($key != $this->idRetailPrice) && ($key != $this->idWholesalePrice)) {
                    $resPricesProduct[] = $value['ID'];
                }
            }

            CPrice::DeleteByProduct($arElement['ID'], $resPricesProduct);
        }
    }

    /**
     * Функция записывает количество на складе от поставщика для товара в список складов для товара на Трейде.
     * @param int $tradeProdID
     * @param array $dataProdBioshop
     */
    public function setCountProductForCurProvider($tradeProdID, $dataProdBioshop) {
        $parse = new Parse();

        $dataStorage = $parse->getDataStorageById($this->idStore);

        if(isset($dataStorage['ID'])) {
            $dataArray = Array(
                "PRODUCT_ID" => $tradeProdID,
                "STORE_ID" => $dataStorage['ID'],
                "AMOUNT" => $dataProdBioshop['amount'],
            );

            $dataStorageForProd = $parse->getDataStorageForProd($tradeProdID, $this->idStore);

            if(!empty($dataStorageForProd)) {
                if(!CCatalogStoreProduct::Update($dataStorageForProd['ID'], $dataArray)) {
                    return false;
                }
            } else {
                if(!CCatalogStoreProduct::Add($dataArray)) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Функция записывает все цены для товара от поставщика в список цен товара на Трейде.
     * @param int $tradeProdID
     * @param array $dataProdBioshop
     */
    public function setPriceProductForCurProvider($tradeProdID, $dataProdBioshop) {
        $obPrice = new CPrice();
        $parse = new Parse();
        $pricesProduct = $parse->getPricesProduct($tradeProdID);

        $dataArray = array(
            "PRODUCT_ID" => $tradeProdID,
        );

        //$dataArray['CURRENCY'] = in_array($dataProdBioshop['currency'], array('EUR', 'USD')) ? $dataProdBioshop['currency'] : 'RUB';

        if(isset($dataProdBioshop['price_dealers']) && !empty($dataProdBioshop['price_dealers'])) {
            $dataArray['CATALOG_GROUP_ID'] = $this->idDealersPrice;
            $dataArray['PRICE'] = $parse->parseCatalogPriceFormat($dataProdBioshop['price_dealers']);
            $dataArray['CURRENCY'] = in_array($dataProdBioshop['currency'], array('EUR', 'USD')) ? $dataProdBioshop['currency'] : 'RUB';

            if (array_key_exists($this->idDealersPrice, $pricesProduct)) {
                if (!$obPrice->Update($pricesProduct[$this->idDealersPrice]['ID'], $dataArray)) {
                    return false;
                }
            } else {
                if (!$obPrice->Add($dataArray)) {
                    return false;
                }
            }
        }

        if(isset($dataProdBioshop['price_retail']) && !empty($dataProdBioshop['price_retail'])) {
            $dataArray['CATALOG_GROUP_ID'] = $this->idRetailPrice;
            $dataArray['PRICE'] = $parse->parseCatalogPriceFormat($dataProdBioshop['price_retail']);
            $dataArray['CURRENCY'] = in_array($dataProdBioshop['currency'], array('EUR', 'USD')) ? $dataProdBioshop['currency'] : 'RUB';

            if(array_key_exists($this->idRetailPrice, $pricesProduct)) {
                if(!$obPrice->Update($pricesProduct[$this->idRetailPrice]['ID'], $dataArray)) {
                    return false;
                }
            } else {
                if(!$obPrice->Add($dataArray)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Функция записывает логи по найденным и не найденным товарам.
     * @param string $statisticPath
     * @param array $product
     * @param object $cbr
     * @param string $status
     */
    function statisticFoundProducts($statisticPath, $product, $cbr, $status) {
        if (!empty($statisticPath)) {
            $fStat = fopen($statisticPath,'a+');

            if($fStat) {
                $dataSave = array(
                    $product['article'],
                    $product['old_article'],
                    $product['name'],
                    $product['vendor'],
                    $product['model'],
                    str_replace(',', '.', $product['price_dealers']),
                    str_replace(',', '.', $product['price_retail']),
                    $product['currency'],
                    print_r($cbr, 1),
                    $product['amount'],
                    $status,
                    date("H:i:s d.m.Y")
                );

                fputcsv($fStat, array_map(array('Parse', 'convert_utf_in_cp'), $dataSave), ';', '"');
                fclose($fStat);
            }
        }
    }
}

$parser = new ParserBioshop();

$parser->startParser();

?>