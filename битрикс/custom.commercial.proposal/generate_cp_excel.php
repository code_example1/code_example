<?

use Bitrix\Sale;

require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/classes/CBRAgent.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/classes/Parse.php');

$cbr = new CBRAgent(); // класс получения курса валют от ЦБ
$parse = new Parse();
$iblockId = 25; // id основного инфоблока
$iblockCPId = 37; // id инфоблока со списком КП

$result = [];
$postData = $_POST;

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

$basketItems = $basket->getBasketItems();

foreach ($basketItems as $item) {
    $arFilter = array("IBLOCK_ID"=>25, 'ID' => $item->getProductId());
    $arSelectFields = array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND.NAME", "PROPERTY_STRANA_PROIZVODITEL", "PROPERTY_PROVIDER_FOR_COEFFICIENT");
    $rsElements = CIBlockElement::GetList(FALSE, $arFilter, FALSE, FALSE, $arSelectFields);
    $arProizvoditel = [];
    if ($arElement = $rsElements->GetNext()) {
        $detailPageURL = $arElement['DETAIL_PAGE_URL'];
        if (!empty($arElement['PROPERTY_BRAND_NAME'])) {
            $arProizvoditel[] = $arElement['PROPERTY_BRAND_NAME'];
        }
        if (!empty($arElement['PROPERTY_STRANA_PROIZVODITEL_VALUE'])) {
            $arProizvoditel[] = $arElement['PROPERTY_STRANA_PROIZVODITEL_VALUE'];
        }

        $imgData = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array("width"=>130, 'height' => 130),BX_RESIZE_IMAGE_PROPORTIONAL, true);

        if(!empty($arElement['PROPERTY_PROVIDER_FOR_COEFFICIENT_VALUE'])) {
            $rsProvider = CIBlockSection::GetList(array('ID' => 'ASC'), array('IBLOCK_ID' => 32, 'ID' => $arElement['PROPERTY_PROVIDER_FOR_COEFFICIENT_VALUE']));
            if ($arProvider = $rsProvider->GetNext()) {
                $provider = $arProvider['NAME'];
            }
        }
    }

    $strProizvoditel = '';
    if (count($arProizvoditel) > 0) {
        $strProizvoditel = implode(' ', $arProizvoditel);
    }

    if(isset($imgData['src'])) {
        $img = $imgData['src'];
    } else {
        $img = '/bitrix/templates/aspro_next/images/no_photo_medium.png';
    }

    $result['PRODUCTS'][$item->getProductId()] = array(
        'IMAGE' => $img,
        'NAME' => $item->getField('NAME'),
        'DETAIL_PAGE_URL' => 'https://' . $_SERVER['HTTP_HOST'] . $detailPageURL,
        'DISCOUNT' => $item->getDiscountPrice() * 100,
        'PRICE' => $item->getPrice(),
        'QUANTITY' => $item->getQuantity(),
        'TOTAL_PRICE' => $item->getFinalPrice(),
        'CAN_BUY' => $item->canBuy(),
        'CURRENCY' => $item->getCurrency(),
        'MEASURE_NAME' => $item->getField('MEASURE_NAME'),
        'PROIZVODITEL' => $strProizvoditel,
        'PROVIDER' => isset($provider) ? $provider : '',
        'PRODUCT_ID' => $item->getProductId(),
    );



    /* Получаем минимальную дилерскую цену для товара (НАЧАЛО) */

    $arArticles = [];
    $inStockProducer = [];
    $dealerPrice = '-';
    $minPriceAndCount = [];
    $arPricesOld = array();
    $arPricesNew = array();


    $productId = $item->getProductId();
    $resProp = CIBlockElement::GetProperty($iblockId, $productId);

    while ($obProp = $resProp->GetNext()) {
        if((strripos($obProp['CODE'], 'ARTICLE_') !== false) && !empty($obProp['VALUE'])) {
            $arArticles[] = $obProp['CODE'];
        }
    }

    if(!empty($arArticles)) {
        $res = CIBlockElement::GetProperty($iblockId, $productId, 'ID', 'DESC', array('ID' => 1391));

        while ($ob = $res->GetNext()) {
            $issetArticle = false;

            foreach ($arArticles as $article) {
                if(strripos($article, $ob['VALUE_XML_ID']) !== false) {
                    $issetArticle = true;

                    break;
                }
            }

            if($issetArticle) {
                $inStockProducer[] = array(
                    'ID' => $ob['VALUE'],
                    'VALUE_XML_ID' => $ob['VALUE_XML_ID'],
                );
            }
        }
    }

    if(!empty($inStockProducer)) {
        $arFilter = array("PRODUCT_ID" => $productId, '!CATALOG_GROUP_ID' => 1, '!PRICE' => array(false, 0));

        $arSelectFields = array("ID", "CATALOG_GROUP_NAME", "CATALOG_GROUP_ID", "PRICE", "CURRENCY");
        $rsPrice = CPrice::GetListEx(array(), $arFilter, false, false, $arSelectFields);

        while ($dataPrice = $rsPrice->Fetch()) {
            $arPricesOld[$dataPrice['ID']] = $dataPrice;
        }

        if(!empty($arPricesOld)) {
            if($cbr->load()) {
                foreach ($arPricesOld as $priceId => $dataPrice) {
                    if ($dataPrice['CURRENCY'] != 'RUB') {
                        if(is_array($cbr)) {
                            $cbrExRate = isset($cbr[$dataPrice['CURRENCY']]) ? $cbr[$dataPrice['CURRENCY']] : 0;
                        } else {
                            $cbrExRate = $cbr->get($dataPrice['CURRENCY']);
                        }

                        $dataPrice['PRICE'] = $dataPrice['PRICE'] * $cbrExRate;
                        $dataPrice['CURRENCY'] = 'RUB';

                        $arPricesNew[$priceId] = $dataPrice;
                    } else {
                        $arPricesNew[$priceId] = $dataPrice;
                    }
                }
            }

            if(!empty($arPricesNew)) {
                foreach ($arPricesNew as $key => $price) {
                    $dbPriceType = CCatalogGroup::GetList(array(), array('ID' => $price['CATALOG_GROUP_ID']), false, false, array('ID', 'NAME'));

                    if ($arPriceType = $dbPriceType->Fetch()) {
                        if (!empty($arPriceType['NAME'])) {
                            $priceCode = $arPriceType['NAME'];
                            $partsName = explode('__', $priceCode);
                            $vendor = $partsName[0];

                            if (array_search(strtolower($vendor), array_map('strtolower', array_column($inStockProducer, 'VALUE_XML_ID'))) === false) {
                                unset($arPricesNew[$key]);
                            } else {
                                $arPricesNew[$key]['PRICE_CODE'] = $priceCode;
                            }
                        } else {
                            unset($arPricesNew[$key]);
                        }
                    } else {
                        unset($arPricesNew[$key]);
                    }
                }
            }

            if(!empty($arPricesNew)) {
                $arPricesNewTemp = $arPricesNew;
                $arPricesNew = array();

                foreach ($arPricesNewTemp as $keyTemp=>$priceTemp) {
                    if(strripos($priceTemp['PRICE_CODE'], '_rub') === false) {
                        $arPricesNew[$keyTemp] = $priceTemp;
                    }
                }

                foreach ($arPricesNewTemp as $keyTemp=>$priceTemp) {
                    if(empty($arPricesNew)) {
                        $arPricesNew[$keyTemp] = $priceTemp;
                    } else {
                        $issetInArPricesNew = false;

                        foreach ($arPricesNew as $keyNew=>$priceNew) {
                            if(strripos($priceTemp['PRICE_CODE'], $priceNew['PRICE_CODE']) !== false) {
                                $issetInArPricesNew = true;
                                break;
                            }
                        }

                        if($issetInArPricesNew === false) {
                            $arPricesNew[$keyTemp] = $priceTemp;
                        }
                    }
                }
            }

            if(!empty($arPricesNew)) {
                $minPriceAndCount = $parse->getMinPriceAndCount($productId, $arPricesNew);
            }

            if(isset($minPriceAndCount['PRICE'])) {
                $dealerPrice = trim($minPriceAndCount['PRICE']);
                $dealerPrice = str_replace(",", ".", $dealerPrice);
                $dealerPrice = preg_replace("/\.{1}$/", "", $dealerPrice);
                $dealerPrice = preg_replace('/[^0-9.]/', "", $dealerPrice);

                if(empty($dealerPrice)) {
                    $dealerPrice = 0;
                }

                $result['PRODUCTS'][$productId]['DEALER_PRICE'] = round($dealerPrice, 2);
            }
        }
    }

    /* Получаем минимальную дилерскую цену для товара (КОНЕЦ) */

}

$firstProductInBasket = current($result['PRODUCTS']);

$result['TOTAL_SUM_ORDER'] = CCurrencyLang::CurrencyFormat($basket->getPrice(), $firstProductInBasket['CURRENCY']);

if(!empty($result['PRODUCTS'])) {
    if(isset($postData['price_product'])
        && !empty($postData['price_product'])) {
        foreach ($postData['price_product'] as $idProd => $productPrice) {
            if(array_key_exists($idProd, $result['PRODUCTS'])) {
                if($productPrice != CCurrencyLang::CurrencyFormat($result['PRODUCTS'][$idProd]['PRICE'], $result['PRODUCTS'][$idProd]['CURRENCY'])) {
                    $productPrice = trim($productPrice);
                    $productPrice = str_replace(",", ".", $productPrice);
                    $productPrice = preg_replace("/\.{1}$/", "", $productPrice);
                    $productPrice = preg_replace('/[^0-9.]/', "", $productPrice);

                    if(empty($productPrice)) {
                        $productPrice = 0;
                    }

                    $result['PRODUCTS'][$idProd]['PRICE'] = $productPrice;
                }
            }
        }
    }

    if(isset($postData['quantity_product'])
        && !empty($postData['quantity_product'])) {
        foreach ($postData['quantity_product'] as $idProd => $productQuantity) {
            if(array_key_exists($idProd, $result['PRODUCTS'])) {
                if($productQuantity != $result['PRODUCTS'][$idProd]['QUANTITY'] . ' ' . $result['PRODUCTS'][$idProd]['MEASURE_NAME']) {
                    $productQuantity = (int) $productQuantity;

                    $result['PRODUCTS'][$idProd]['QUANTITY'] = $productQuantity;
                }
            }
        }
    }

    if(isset($postData['status_product'])
        && !empty($postData['status_product'])) {
        foreach ($postData['status_product'] as $idProd => $productStatus) {
            if(array_key_exists($idProd, $result['PRODUCTS'])) {
                $result['PRODUCTS'][$idProd]['STATUS'] = $productStatus;
            }
        }
    }

    $order = $postData['id_basketItem'];

    usort($result['PRODUCTS'], function ($a, $b) use ($order) {
        $pos_a = array_search($a['PRODUCT_ID'], $order);
        $pos_b = array_search($b['PRODUCT_ID'], $order);
        return $pos_a - $pos_b;
    });

    $arMainProps = [
        'STRANA_PROIZVODITEL',
        'PODKLYUCHENIE',

        'GABARITNYE_RAZMERY_MM',
        'MOSHCHNOST',
        'VES_BEZ_UPAKOVKI', //

        'VES_OBSHCHIY_BEZ_UPAKOVKI',
        'VES_KG',

        'VES_S_UPAKOVKOY',
        'VES_OBSHCHIY_S_UPAKOVKOY',
        'VES_V_UPAKOVKE_KG'


    ];

    $arProdutcsIds = [];
    foreach ($result['PRODUCTS'] as $idProduct => $arProduct) {
        $arProdutcsIds[] = $idProduct;
    }

    if (!empty($arProdutcsIds) && count($arProdutcsIds) > 0) {
        $arSelect = [];
        $arFilter = ['ID' => $arProdutcsIds];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        $result['PRODUCT_PROPS'] = [];

        while ($ob = $res->GetNextElement()) {
            $arPropsElem = $ob->GetProperties();

            $arFields = $ob->GetFields();
            if (!empty($arPropsElem['SHIRINA']['VALUE']) && !empty($arPropsElem['GLUBINA']['VALUE']) && !empty($arPropsElem['VYSOTA']['VALUE'])) {
                $result['PRODUCT_PROPS'][$arFields['ID']][] =  'Размеры - ' . $arPropsElem['SHIRINA']['VALUE'] . 'x' . $arPropsElem['GLUBINA']['VALUE'] . 'x' .$arPropsElem['VYSOTA']['VALUE'];
            }
            foreach ($arMainProps as $propMain) {
                $arProp = $arPropsElem[$propMain];
                if (!empty($arProp['VALUE'])) {
                    switch ($arProp['CODE']) {

                        case 'VES_BEZ_UPAKOVKI':

                            //$arProp['NAME'] = str_replace(['(', ')'], ['', ''], $arProp['NAME']);
                            //$arProp['VALUE'] = str_replace(['(', ')'], ['', ''], $arProp['VALUE']);
                            //$arProp['VALUE'] = str_replace('.', ',', $arProp['VALUE']);

                            $result['PRODUCT_PROPS'][$arFields['ID']][] = $arProp['NAME'] . ' - ' . $arProp['VALUE'];
                            break;

                        case 'STRANA_PROIZVODITEL':
                            //$arProp['VALUE'] = str_replace('.', ' ', $arProp['VALUE']);
                            $result['PRODUCT_PROPS'][$arFields['ID']][] = $arProp['VALUE'];

                            break;
                        case 'PODKLYUCHENIE':
                            $result['PRODUCT_PROPS'][$arFields['ID']][] = $arProp['NAME'] . ' - ' . implode(', ', $arProp['VALUE']);
                            break;
                        default:
                            //$arProp['NAME'] = str_replace(['(', ')'], ['', ''], $arProp['NAME']);
                            //$arProp['VALUE'] = str_replace(['(', ')'], ['', ''], $arProp['VALUE']);
                            //$arProp['VALUE'] = str_replace('.', ',', $arProp['VALUE']);

                            $result['PRODUCT_PROPS'][$arFields['ID']][] = $arProp['NAME'] . ' - ' . $arProp['VALUE'];

                            //echo "<pre>"; var_export($arProp['NAME'] . ' - ' . $arProp['VALUE']); echo "</pre>";

                            break;
                    }

                }
            }
        }
    }

    if(isset($postData['number-cp'])
        && !empty($postData['number-cp'])) {
        $result['NUMBER'] = $postData['number-cp'];
    }

    if(isset($postData['additional_inf'])
        && !empty($postData['additional_inf'])) {
        $result['ADDITIONAL_INF'] = $postData['additional_inf'];
    }

    if(isset($postData['date-cp'])
        && !empty($postData['date-cp'])) {
        $result['DATE'] = $postData['date-cp'];
    }

    require_once('template_cp_excel.php');
}