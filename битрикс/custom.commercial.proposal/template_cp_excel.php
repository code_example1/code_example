<?
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', '6000');
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/lib/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$sheet = $objPHPExcel->getActiveSheet();
// Set document properties



// header

//логотип
$style = array(
    'font' => array(
        'name'      => 'Calibri',
        'size'      => 14,
        'bold'      => true,  //шрифт
    )
);

$nameFile = "Коммерческое предложение №". $result['NUMBER'] ." от ". $result['DATE'];

$sheet->setCellValue("C6", $nameFile);
$sheet->getStyle("C6")->applyFromArray($style);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setResizeProportional(false);
$objDrawing->setPath($_SERVER['DOCUMENT_ROOT'] . "/upload/CNext/2c2/2c226d0482d2461bf614f1e2a43dd767.png");
$objDrawing->setCoordinates("B2");
$objDrawing->setOffsetX(30);
$objDrawing->setOffsetY(0);
$objDrawing->setWidth(395);
$objDrawing->setHeight(70);
$objDrawing->setWorksheet($sheet);

//номер телефона
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setResizeProportional(false);
$objDrawing->setPath($_SERVER['DOCUMENT_ROOT'] . "/upload/images/td-excel-phone.png");
$objDrawing->setCoordinates("H2");
$objDrawing->setOffsetX(70);
$objDrawing->setOffsetY(10);
$objDrawing->setWidth(373);
$objDrawing->setHeight(65);
$objDrawing->setWorksheet($sheet);


//table title
$bg = array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'd9d9d9') //фон
    ),
    'font' => array(
        'name'      => 'Calibri',
        'size'      => 12,
        'bold'      => true,  //шрифт
    ),
    'borders'=>array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000')  //граница
        ),
    )
);

$arHeaderColums = ["B8","C8", "D8", "E8", "F8", "G8", "H8", "I8", "J8", "K8", "L8"];  //применяем наши стили для заголовка
foreach ($arHeaderColums as $colum) {
    $sheet->getStyle($colum)->applyFromArray($bg);
    $sheet->getStyle($colum)->getAlignment()->setWrapText(true);
    $sheet->getStyle($colum)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
}

$sheet->getRowDimension("8")->setRowHeight(50);  //высоту ячеек заголовков
$sheet->mergeCells("D8:E8");

$sheet->getColumnDimension("B")->setWidth(21);  //ширина каждой ячейки
$sheet->getColumnDimension("C")->setWidth(6);
$sheet->getColumnDimension("D")->setWidth(21);
$sheet->getColumnDimension("E")->setWidth(36);
$sheet->getColumnDimension("F")->setWidth(17);
$sheet->getColumnDimension("G")->setWidth(17);
$sheet->getColumnDimension("H")->setWidth(12);
$sheet->getColumnDimension("I")->setWidth(17);
$sheet->getColumnDimension("J")->setWidth(15);
$sheet->getColumnDimension("K")->setWidth(16);
$sheet->getColumnDimension("L")->setWidth(11);

//заполнил заголовки
$sheet->setCellValue("B8", "Поставщик");
$sheet->setCellValue("C8", "№");
$sheet->setCellValue("D8", "Наименование");
$sheet->setCellValue("F8", "Производитель");
$sheet->setCellValue("G8", "Кол-во, шт");
$sheet->setCellValue("H8", "Цена диллерская, Руб");
$sheet->setCellValue("I8", "Сумма дилерская, Руб");
$sheet->setCellValue("J8", "Цена Розничная, Руб");
$sheet->setCellValue("K8", "Сумма Розничная, Руб");
$sheet->setCellValue("L8", "Статус товара");

$sheet->getDefaultStyle()->getFont()->setName('Calibri');
$sheet->getDefaultStyle()->getFont()->setSize(12);

$style = array(
    'borders'=>array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000')  //граница
        ),
    )
);




// ITEMS

$i = 1;
$rowNum = 9;

$arCols = ["B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"];

$htmlHelper = new \PHPExcel_Helper_HTML();

foreach ($result['PRODUCTS'] as $idProduct => $arProduct) {
    if (!empty($arProduct["NAME"])) {

        if(!empty($arProduct['PROVIDER'])) {
            $sheet->setCellValue("B" . $rowNum, $arProduct['PROVIDER']);
        } else {
            $sheet->setCellValue("B" . $rowNum, $arProduct['PROIZVODITEL']);
        }
        
        $sheet->setCellValue("C" . $rowNum, $i);

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setResizeProportional(false);
        $objDrawing->setPath($_SERVER['DOCUMENT_ROOT'] . $arProduct['IMAGE']);
        $objDrawing->setCoordinates("D" . $rowNum);
        $objDrawing->setOffsetX(10);
        $objDrawing->setOffsetY(10);
        //$objDrawing->setWidth(163);
        //$objDrawing->setHeight(50);
        $objDrawing->setWorksheet($sheet);

        $html = '';
        $html .= $arProduct["NAME"] . '' ."\n\n";

        if (!empty( $result['PRODUCT_PROPS'][$idProduct])) {
            $props = $result['PRODUCT_PROPS'][$idProduct];
            $html .= 'Технические характеристики: ' . "\n";

            $html .= implode("\n", $props);
        }

        $html = str_replace('.', ',', $html);

        //echo $html;
        //echo "<br>";

        //$rich_text = $htmlHelper->toRichTextObject(mb_convert_encoding(html_entity_decode($html), 'HTML-ENTITIES', 'UTF-8'));

        $sheet->setCellValue("E" . $rowNum, $html);
        //$sheet->setCellValue("D" . $rowNum, $rich_text);

        $sheet->setCellValue("F" . $rowNum, $arProduct['PROIZVODITEL']);
        $sheet->setCellValue("G" . $rowNum, $arProduct["QUANTITY"]);
        $sheet->setCellValue("H" . $rowNum, $arProduct["DEALER_PRICE"]); //Цена диллерская

        $sheet->setCellValue("I" . $rowNum, "=G" . $rowNum ."*H" . $rowNum);  // Сумма дилерская
        $sheet->setCellValue("J" . $rowNum, $arProduct["PRICE"]);  //цена


        $sheet->setCellValue("K" . $rowNum, "=G" . $rowNum ."*J" . $rowNum);  //цена сумма

        $sheet->setCellValue("L" . $rowNum, $arProduct["STATUS"]);

        foreach($arCols as $colWord) {
            $sheet->getStyle($colWord . $rowNum)->applyFromArray($style);
            $sheet->getStyle($colWord . $rowNum)->getAlignment()->setWrapText(true);
            $sheet->getStyle($colWord . $rowNum)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
        }
        $sheet->getRowDimension($rowNum)->setRowHeight(115);  //высоту ячеек заголовков

        $rowNum++;
        $i++;
    }
}

//die();

$endItemRow = $rowNum - 1;
// TOTAL
$rowTotal = $rowNum + 1;


$arColsTotal = ["G", "H", "I", "J"];
$style = array(
    'font' => array(
        'name'      => 'Calibri',
        'size'      => 12,
        'bold'      => true,
    ),

);
foreach ($arColsTotal as $colum) {
    $sheet->getStyle($colum . $rowTotal)->applyFromArray($style);
    $sheet->getStyle($colum . $rowTotal)->getAlignment()->setWrapText(true);
    $sheet->getStyle($colum . $rowTotal)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
}


$sheet->setCellValue("H" . $rowTotal, "Итого дилерская:");
$sheet->setCellValue("I" . $rowTotal, "=SUM(H9:H". $endItemRow .")");
$sheet->setCellValue("J" . $rowTotal, "Итого розница:");
$sheet->setCellValue("K" . $rowTotal, "=SUM(J9:J". $endItemRow .")");

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Kp');


$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$nameFile.'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

//echo "<pre>"; var_export($result['PRODUCTS']); echo "</pre>";