<?

use Bitrix\Sale;

$result = [];
$custonBasketItems = [];
$postData = $_POST;

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

$basketItems = $basket->getBasketItems();

foreach ($basketItems as $item) {
    $custonBasketItems['PRODUCTS'][$item->getProductId()] = array(
        'NAME' => $item->getField('NAME'),
        'PRICE' => $item->getPrice(),
        'QUANTITY' => $item->getQuantity(),
        'TOTAL_PRICE' => $item->getFinalPrice(),
        'CURRENCY' => $item->getCurrency(),
        'MEASURE_NAME' => $item->getField('MEASURE_NAME'),
    );
}

$firstProductInBasket = current($custonBasketItems['PRODUCTS']);

$custonBasketItems['TOTAL_SUM_ORDER'] = CCurrencyLang::CurrencyFormat($basket->getPrice(), $firstProductInBasket['CURRENCY']);

if(!empty($custonBasketItems['PRODUCTS'])) {
    if(isset($postData['arPrices'])
        && !empty($postData['arPrices'])) {
        foreach ($postData['arPrices'] as $idProd => $productPrice) {
            if(array_key_exists($idProd, $custonBasketItems['PRODUCTS'])) {
                if($productPrice != CCurrencyLang::CurrencyFormat($custonBasketItems['PRODUCTS'][$idProd]['PRICE'], $custonBasketItems['PRODUCTS'][$idProd]['CURRENCY'])) {
                    $productPrice = trim($productPrice);
                    $productPrice = str_replace(",", ".", $productPrice);
                    $productPrice = preg_replace("/\.{1}$/", "", $productPrice);
                    $productPrice = preg_replace('/[^0-9.]/', "", $productPrice);

                    if(empty($productPrice)) {
                        $productPrice = 0;
                    }

                    $custonBasketItems['PRODUCTS'][$idProd]['PRICE'] = $productPrice;
                }
            }
        }
    }

    if(isset($postData['arQuantities'])
        && !empty($postData['arQuantities'])) {
        foreach ($postData['arQuantities'] as $idProd => $productQuantity) {
            if(array_key_exists($idProd, $custonBasketItems['PRODUCTS'])) {
                if($productQuantity != $custonBasketItems['PRODUCTS'][$idProd]['QUANTITY'] . ' ' . $custonBasketItems['PRODUCTS'][$idProd]['MEASURE_NAME']) {
                    $productQuantity = (int) $productQuantity;

                    $custonBasketItems['PRODUCTS'][$idProd]['QUANTITY'] = $productQuantity;
                }
            }
        }
    }

    foreach ($custonBasketItems['PRODUCTS'] as $idProduct => $arProduct) {
        $custonBasketItems['PRODUCTS'][$idProduct]['TOTAL_PRICE'] = $custonBasketItems['PRODUCTS'][$idProduct]['PRICE'] * $custonBasketItems['PRODUCTS'][$idProduct]['QUANTITY'];
    }

    if(isset($custonBasketItems['PRODUCTS'][$postData['needProductId']])) {
        $custonBasketItems['TOTAL_SUM_ORDER'] = 0;

        foreach ($custonBasketItems['PRODUCTS'] as $idProduct => $arProduct) {
            $custonBasketItems['TOTAL_SUM_ORDER'] = $custonBasketItems['TOTAL_SUM_ORDER'] + $arProduct['TOTAL_PRICE'];
        }

        $result['totalSumOrder'] = $custonBasketItems['TOTAL_SUM_ORDER'];

        $custonBasketItems['TOTAL_SUM_ORDER'] = CCurrencyLang::CurrencyFormat($custonBasketItems['TOTAL_SUM_ORDER'], $firstProductInBasket['CURRENCY']);

        $result['totalSumProduct'] = CCurrencyLang::CurrencyFormat($custonBasketItems['PRODUCTS'][$postData['needProductId']]['TOTAL_PRICE'], $custonBasketItems['PRODUCTS'][$postData['needProductId']]['CURRENCY']);

        $result['totalSumOrderWithMeasure'] = $custonBasketItems['TOTAL_SUM_ORDER'];

        $result['status'] = 'ok';
    } else {
        $result['status'] = 'error';
    }
} else {
    $result['status'] = 'error';
}

echo json_encode($result);
exit();