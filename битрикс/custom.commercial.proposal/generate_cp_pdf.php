<?

use Bitrix\Sale;

$result = [];
$postData = $_POST;

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

$basketItems = $basket->getBasketItems();

foreach ($basketItems as $item) {
    $arFilter = array("IBLOCK_ID"=>25, 'ID' => $item->getProductId());
    $arSelectFields = array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL");
    $rsElements = CIBlockElement::GetList(FALSE, $arFilter, FALSE, FALSE, $arSelectFields);

    if ($arElement = $rsElements->GetNext()) {
        $detailPageURL = $arElement['DETAIL_PAGE_URL'];

        $imgData = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array("width"=>130, 'height' => 130),BX_RESIZE_IMAGE_PROPORTIONAL, true);
    }

    if(isset($imgData['src'])) {
        $img = $imgData['src'];
    } else {
        $img = '/bitrix/templates/aspro_next/images/no_photo_medium.png';
    }

    $result['PRODUCTS'][$item->getProductId()] = array(
        'IMAGE' => $img,
        'NAME' => $item->getField('NAME'),
        'DETAIL_PAGE_URL' => 'https://' . $_SERVER['HTTP_HOST'] . $detailPageURL,
        'DISCOUNT' => $item->getDiscountPrice() * 100,
        'PRICE' => $item->getPrice(),
        'QUANTITY' => $item->getQuantity(),
        'TOTAL_PRICE' => $item->getFinalPrice(),
        'CAN_BUY' => $item->canBuy(),
        'CURRENCY' => $item->getCurrency(),
        'MEASURE_NAME' => $item->getField('MEASURE_NAME'),
        'PRODUCT_ID' => $item->getProductId(),
    );
}


$firstProductInBasket = current($result['PRODUCTS']);

$result['TOTAL_SUM_ORDER'] = CCurrencyLang::CurrencyFormat($basket->getPrice(), $firstProductInBasket['CURRENCY']);

if(!empty($result['PRODUCTS'])) {
    if(isset($postData['price_product'])
        && !empty($postData['price_product'])) {
        foreach ($postData['price_product'] as $idProd => $productPrice) {
            if(array_key_exists($idProd, $result['PRODUCTS'])) {
                if($productPrice != CCurrencyLang::CurrencyFormat($result['PRODUCTS'][$idProd]['PRICE'], $result['PRODUCTS'][$idProd]['CURRENCY'])) {
                    $productPrice = trim($productPrice);
                    $productPrice = str_replace(",", ".", $productPrice);
                    $productPrice = preg_replace("/\.{1}$/", "", $productPrice);
                    $productPrice = preg_replace('/[^0-9.]/', "", $productPrice);

                    if(empty($productPrice)) {
                        $productPrice = 0;
                    }

                    $result['PRODUCTS'][$idProd]['PRICE'] = $productPrice;
                }
            }
        }
    }

    if(isset($postData['quantity_product'])
        && !empty($postData['quantity_product'])) {
        foreach ($postData['quantity_product'] as $idProd => $productQuantity) {
            if(array_key_exists($idProd, $result['PRODUCTS'])) {
                if($productQuantity != $result['PRODUCTS'][$idProd]['QUANTITY'] . ' ' . $result['PRODUCTS'][$idProd]['MEASURE_NAME']) {
                    $productQuantity = (int) $productQuantity;

                    $result['PRODUCTS'][$idProd]['QUANTITY'] = $productQuantity;
                }
            }
        }
    }

    if(isset($postData['status_product'])
        && !empty($postData['status_product'])) {
        foreach ($postData['status_product'] as $idProd => $productStatus) {
            if(array_key_exists($idProd, $result['PRODUCTS'])) {
                $result['PRODUCTS'][$idProd]['STATUS'] = $productStatus;
            }
        }
    }

    foreach ($result['PRODUCTS'] as $idProduct => $arProduct) {
        $result['PRODUCTS'][$idProduct]['TOTAL_PRICE'] = $result['PRODUCTS'][$idProduct]['PRICE'] * $result['PRODUCTS'][$idProduct]['QUANTITY'];
    }

    $result['TOTAL_SUM_ORDER'] = 0;

    foreach ($result['PRODUCTS'] as $idProduct => $arProduct) {
        $result['TOTAL_SUM_ORDER'] = $result['TOTAL_SUM_ORDER'] + $arProduct['TOTAL_PRICE'];
    }

    $result['TOTAL_SUM_ORDER'] = CCurrencyLang::CurrencyFormat($result['TOTAL_SUM_ORDER'], $firstProductInBasket['CURRENCY']);

    $order = $postData['id_basketItem'];

    usort($result['PRODUCTS'], function ($a, $b) use ($order) {
        $pos_a = array_search($a['PRODUCT_ID'], $order);
        $pos_b = array_search($b['PRODUCT_ID'], $order);
        return $pos_a - $pos_b;
    });

    $arMainProps = [
        'GABARITNYE_RAZMERY_MM',
        'PODKLYUCHENIE',
        'MOSHCHNOST',
        'VES_BEZ_UPAKOVKI',
        'VES_OBSHCHIY_BEZ_UPAKOVKI',
        'VES_KG',
        'VES_S_UPAKOVKOY',
        'VES_OBSHCHIY_S_UPAKOVKOY',
        'VES_V_UPAKOVKE_KG',
        'STRANA_PROIZVODITEL',
    ];

    $arProdutcsIds = [];
    foreach ($result['PRODUCTS'] as $idProduct => $arProduct) {
        $arProdutcsIds[] = $idProduct;
    }

    if (!empty($arProdutcsIds) && count($arProdutcsIds) > 0) {
        $arSelect = [];
        $arFilter = ['ID' => $arProdutcsIds];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        $result['PRODUCT_PROPS'] = [];

        while ($ob = $res->GetNextElement()) {
            $arPropsElem = $ob->GetProperties();

            $arFields = $ob->GetFields();
            if (!empty($arPropsElem['SHIRINA']['VALUE']) && !empty($arPropsElem['GLUBINA']['VALUE']) && !empty($arPropsElem['VYSOTA']['VALUE'])) {
                $result['PRODUCT_PROPS'][$arFields['ID']][] =  'Размеры - ' . $arPropsElem['SHIRINA']['VALUE'] . 'x' . $arPropsElem['GLUBINA']['VALUE'] . 'x' .$arPropsElem['VYSOTA']['VALUE'];
            }
            foreach ($arMainProps as $propMain) {
                $arProp = $arPropsElem[$propMain];
                if (!empty($arProp['VALUE'])) {
                    switch ($arProp['CODE']) {
                        case 'STRANA_PROIZVODITEL':
                            $result['PRODUCT_PROPS'][$arFields['ID']][] = $arProp['VALUE'];
                            break;
                        case 'PODKLYUCHENIE':
                            $result['PRODUCT_PROPS'][$arFields['ID']][] = $arProp['NAME'] . ' - ' . implode(', ', $arProp['VALUE']);
                            break;
                        default:
                            $result['PRODUCT_PROPS'][$arFields['ID']][] = $arProp['NAME'] . ' - ' . $arProp['VALUE'];
                            break;
                    }

                }
            }
        }
    }

    if(isset($postData['number-cp'])
        && !empty($postData['number-cp'])) {
        $result['NUMBER'] = $postData['number-cp'];
    }

    if(isset($postData['additional_inf'])
        && !empty($postData['additional_inf'])) {
        $result['ADDITIONAL_INF'] = $postData['additional_inf'];
    }

    if(isset($postData['date-cp'])
        && !empty($postData['date-cp'])) {
        $result['DATE'] = $postData['date-cp'];
    }

    require_once('template_cp_pdf.php');
}