<?

require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
$pdf = new TCPDF();


// products html wrapper
$products = '
            <table width="100%" border="0" cellspacing="0" cellpadding="3" style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td colspan="3" style="width: 50%;"><h3>Предварительный список товаров</h3></td>
                        <td colspan="3" style="width: 50%; text-align: right;"><h4>Коммерческое предложение №'. $result['NUMBER'] .' от '. $result['DATE'] .'</h4></td>
                    </tr>
                    <tr>
                        <th colspan="2" style="width: 40%; font-weight: bold; background-color: #eee; border-bottom: 1px solid #333;">Наименование</th>
                        <th style="width: 18%; font-weight: bold; background-color: #eee; border-bottom: 1px solid #333; text-align: right;">Цена</th>
                        <th style="width: 8%; font-weight: bold; background-color: #eee; border-bottom: 1px solid #333;"> Кол-во</th>
                        <th style="width: 18%; font-weight: bold; background-color: #eee; border-bottom: 1px solid #333; text-align: right;">Сумма</th>
                        <th style="width: 16%; font-weight: bold; background-color: #eee; border-bottom: 1px solid #333; text-align: right;">Статус товара</th>
                    </tr>
                </thead>
                <tbody>
        ';

// products html
$i = 0;
foreach ($result['PRODUCTS'] as $idProduct => $arProduct) {
    if(isset($arProduct["DETAIL_PAGE_URL"]) && !empty($arProduct["DETAIL_PAGE_URL"])) {
        $arProduct["NAME"] = '<a href="'. $arProduct["DETAIL_PAGE_URL"] .'">'. $arProduct["NAME"] .'</a>';
    }

    $products .= '
                <tr>
                    <td style="width: 3%; vertical-align: bottom; border-bottom: 1px solid #999;">'. ($i += 1) .'</td>
                    <td style="width: 8%; border-bottom: 1px solid #999;">
                        <img src="' . $arProduct['IMAGE'] . '" alt="">
                    </td>
                    <td style="width: 29%; border-bottom: 1px solid #999;">'. $arProduct["NAME"];

    if(!$arProduct['CAN_BUY']) {
        $products .= '<br><span style="color:red;">Товар отсутствует</span>';
    }

    if (!empty( $result['PRODUCT_PROPS'][$idProduct])) {
        $props = $result['PRODUCT_PROPS'][$idProduct];
        $products .= '<p>Технические характеристики: ';
        $products .= implode(', ', $props);
        $products .= '</p>';
    }

    $products .= '</td>
                    <td style="width: 18%; border-bottom: 1px solid #999; text-align: right;">
                        '. CCurrencyLang::CurrencyFormat($arProduct["PRICE"], $arProduct['CURRENCY']) .'
                    </td>
                    <td style="width: 8%; border-bottom: 1px solid #999;">
                        '. $arProduct["QUANTITY"] .'
                    </td>
                    <td style="width: 18%; border-bottom: 1px solid #999; text-align: right;">
                        <b>'. CCurrencyLang::CurrencyFormat($arProduct["TOTAL_PRICE"], $arProduct['CURRENCY']) .'</b>
                    </td>
                    <td style="width: 16%; border-bottom: 1px solid #999; text-align: right;">
                        '. $arProduct["STATUS"] .'
                    </td>
                </tr>
            ';
}

// products html /wrapper
$products .= '
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7" style="text-align: right; border-top: 3px solid #333;">
                            <b>Итого: <span style="font-size: 11;">' . $result['TOTAL_SUM_ORDER'] . '</span></b>
                        </td>
                    </tr>
                </tfoot>
            </table>
        ';


$pdf->SetCreator('complex');
$pdf->SetTitle('Коммерческое предложение');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();
$pdf->SetFont('freesans', '', 9);

// complete html
$html = '
            <table border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #000;">
                <tr>
                    <td style="vertical-align:top;"><a href="/"><img src="'.$_SERVER["DOCUMENT_ROOT"].'upload/CNext/2c2/2c226d0482d2461bf614f1e2a43dd767.png" style="width:150px;" alt=""></a></td>
                    <td style="text-align:right; vertical-align:top;"><b style="color:#d13535; font-size:18pt;">8-495-542-29-00</b><br><span style="font-size:9pt;">125212, Москва, Кронштадтский б-р, 7а</span></td>
                </tr>
                <tr><td height="10"></td><td></td></tr>
            </table>

            <div style="height: 30px;"></div>

            ' . $products . '

            <div style="height: 30px;"></div>
        ';

if(isset($result['ADDITIONAL_INF'])
    && !empty($result['ADDITIONAL_INF'])) {
    $html .= '
            <div>
                <table border="0" cellspacing="0" cellpadding="8">
                    <tr>
                        <td>' . nl2br($result['ADDITIONAL_INF']) . '</td>
                    </tr>
                </table> 
            </div>
            ';
}

$pdf->writeHTML($html, true, false, true, false, '');

$file_date = date('Y-m-d');

header("Content-Disposition: attachment; filename=\"offer_{$file_date}.pdf\"");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Description: File Transfer");

echo $pdf->Output("offer_{$file_date}.pdf", "I");