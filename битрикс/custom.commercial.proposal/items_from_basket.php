<?

use Bitrix\Sale;

require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/classes/CBRAgent.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/parce/classes/Parse.php');

$cbr = new CBRAgent(); // класс получения курса валют от ЦБ
$parse = new Parse();
$iblockId = 25; // id основного инфоблока
$iblockCPId = 37; // id инфоблока со списком КП

/* Если у нас есть cp_id, то значит мы пытаемся загрузить уже сохраненный КП, а не создаем новое */

if(isset($_GET['cp_id'])
    && !empty($_GET['cp_id'])) {
    $arSelectFields = array('ID',
        'NAME',
        'PROPERTY_ADDITIONAL_INFORMATION',
        'PROPERTY_PRODUCTS_PRICES',
        'PROPERTY_PRODUCTS_QUANTITIES',
        'PROPERTY_PRODUCTS_STATUSES',
        'PROPERTY_NUMBER',
        'CREATED_DATE'
    );
    $rsCP = CIBlockElement::GetList(array('ID' => 'ASC'), array('ID' => $_GET['cp_id'], 'IBLOCK_ID' => $iblockCPId), false, false, $arSelectFields);

    if ($arCP = $rsCP->Fetch()) {
        $arFullDataProdSavedCP = array();

        $res = CIBlockElement::GetProperty($iblockCPId, $arCP['ID'], 'ID', 'DESC', array('CODE' => 'PRODUCTS'));

        while ($ob = $res->GetNext()) {
            $arFullDataProdSavedCP[] = array(
                'ID' => $ob['VALUE']
            );
        }

        if(!empty($arCP['PROPERTY_PRODUCTS_PRICES_VALUE'])) {
            foreach ($arCP['PROPERTY_PRODUCTS_PRICES_VALUE'] as $keyPrice => $price) {
                if(array_key_exists($keyPrice, $arFullDataProdSavedCP)) {
                    $arFullDataProdSavedCP[$keyPrice]['PRICE'] = $price;
                }
            }
        }

        if(!empty($arCP['PROPERTY_PRODUCTS_QUANTITIES_VALUE'])) {
            foreach ($arCP['PROPERTY_PRODUCTS_QUANTITIES_VALUE'] as $keyQuantity => $quantity) {
                if(array_key_exists($keyQuantity, $arFullDataProdSavedCP)) {
                    $arFullDataProdSavedCP[$keyQuantity]['QUANTITY'] = $quantity;
                }
            }
        }

        if(!empty($arCP['PROPERTY_PRODUCTS_STATUSES_VALUE'])) {
            foreach ($arCP['PROPERTY_PRODUCTS_STATUSES_VALUE'] as $keyStatus => $status) {
                if(array_key_exists($keyStatus, $arFullDataProdSavedCP)) {
                    $arFullDataProdSavedCP[$keyStatus]['STATUS'] = $status;
                }
            }
        }

        if(!empty($arFullDataProdSavedCP)) {
            if (CSaleBasket::DeleteAll(Sale\Fuser::getId(), False)) {
                $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

                foreach ($arFullDataProdSavedCP as $product) {
                    $item = $basket->createItem('catalog', $product['ID']);
                    $quantityParts = preg_split("/\s+(?=\S*+$)/", $product['QUANTITY']);

                    $item->setFields(array(
                        'QUANTITY' => (isset($quantityParts[0]) && !empty($quantityParts[0])) ? $quantityParts[0] : 0,
                        'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                        'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
                        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                        'PRICE' => (isset($product['PRICE']) && !empty($product['PRICE'])) ? $product['PRICE'] : 0,
                        'CUSTOM_PRICE' => 'Y',
                        'MEASURE_NAME' => (isset($quantityParts[1]) && !empty($quantityParts[1])) ? $quantityParts[1] : 'шт',
                    ));
                }

                $basket->save();
            }
        }

        $cpId = $arCP['ID'];
        $additionalInf = $arCP['PROPERTY_ADDITIONAL_INFORMATION_VALUE']['TEXT'];
        $numberCP = $arCP['PROPERTY_NUMBER_VALUE'];
        $dateCP = FormatDate("d.m.Y", MakeTimeStamp($arCP['CREATED_DATE']));
    }
}

/* Если у нас есть b_cp_id, то значит мы пытаемся создать новое КП на основе сохраненного старого */

if(isset($_GET['b_cp_id'])
    && !empty($_GET['b_cp_id'])) {
    $arSelectFields = array('ID',
        'NAME',
        'PROPERTY_ADDITIONAL_INFORMATION',
        'PROPERTY_PRODUCTS_QUANTITIES',
        'PROPERTY_NUMBER',
        'CREATED_DATE'
    );
    $rsCP = CIBlockElement::GetList(array('ID' => 'ASC'), array('ID' => $_GET['b_cp_id'], 'IBLOCK_ID' => $iblockCPId), false, false, $arSelectFields);

    if ($arCP = $rsCP->Fetch()) {
        $arFullDataProdSavedCP = array();

        $res = CIBlockElement::GetProperty($iblockCPId, $arCP['ID'], 'ID', 'DESC', array('CODE' => 'PRODUCTS'));

        while ($ob = $res->GetNext()) {
            $arFullDataProdSavedCP[] = array(
                'ID' => $ob['VALUE']
            );
        }

        if(!empty($arCP['PROPERTY_PRODUCTS_QUANTITIES_VALUE'])) {
            foreach ($arCP['PROPERTY_PRODUCTS_QUANTITIES_VALUE'] as $keyQuantity => $quantity) {
                if(array_key_exists($keyQuantity, $arFullDataProdSavedCP)) {
                    $arFullDataProdSavedCP[$keyQuantity]['QUANTITY'] = $quantity;
                }
            }
        }

        if(!empty($arFullDataProdSavedCP)) {
            if (CSaleBasket::DeleteAll(Sale\Fuser::getId(), False)) {
                $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

                foreach ($arFullDataProdSavedCP as $product) {
                    $item = $basket->createItem('catalog', $product['ID']);
                    $quantityParts = preg_split("/\s+(?=\S*+$)/", $product['QUANTITY']);

                    $item->setFields(array(
                        'QUANTITY' => (isset($quantityParts[0]) && !empty($quantityParts[0])) ? $quantityParts[0] : 0,
                        'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                        'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
                        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                        'MEASURE_NAME' => (isset($quantityParts[1]) && !empty($quantityParts[1])) ? $quantityParts[1] : 'шт',
                    ));
                }

                $basket->save();
            }
        }

        $additionalInf = $arCP['PROPERTY_ADDITIONAL_INFORMATION_VALUE']['TEXT'];
    }
}

/* Получаем данные для КП из текущей корзины */

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$basketItems = $basket->getBasketItems();

if(count($basketItems) > 0) {
    foreach ($basketItems as $basketItem) {
        $productId = $basketItem->getProductId();

        $arFilter = array("IBLOCK_ID"=>$iblockId, 'ID' => $productId);
        $arSelectFields = array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IN_STOCK", "PROPERTY_REMOVED_FROM_PRODUCTION");
        $rsElements = CIBlockElement::GetList(FALSE, $arFilter, FALSE, FALSE, $arSelectFields);

        if ($arElement = $rsElements->GetNext()) {
            $prodName = $arElement['NAME'];

            $detailPageURL = $arElement['DETAIL_PAGE_URL'];

            $imgData = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array("width"=>130, 'height' => 130), BX_RESIZE_IMAGE_PROPORTIONAL, true);

            if(isset($arElement['PROPERTY_REMOVED_FROM_PRODUCTION_VALUE'])
                && ($arElement['PROPERTY_REMOVED_FROM_PRODUCTION_VALUE'] == 1)) {
                $productIsRemoved = 'Y';
            } else {
                $productIsRemoved = 'N';
            }

            if(isset($arElement['PROPERTY_IN_STOCK_ENUM_ID'])
                && !empty($arElement['PROPERTY_IN_STOCK_ENUM_ID'])) {
                $productInStock = 'Y';
            } else {
                $productInStock = 'N';
            }
        }

        if(!isset($prodName)) {
            $prodName = '';
        }

        if(isset($imgData['src'])) {
            $img = $imgData['src'];
        } else {
            $img = '/bitrix/templates/aspro_next/images/no_photo_medium.png';
        }

        if(isset($detailPageURL) && !empty($detailPageURL)) {
            $prodNameWithURL = '<a href="'. $detailPageURL .'">'. $prodName .'</a>';
        } else {
            $prodNameWithURL = $prodName;
        }

        $productInStock = isset($productInStock) ? $productInStock : 'N';
        $productIsRemoved = isset($productIsRemoved) ? $productIsRemoved : 'Y';

        $productStatus = '';

        if(!empty($arFullDataProdSavedCP)) {
            foreach ($arFullDataProdSavedCP as $product) {
                if($product['ID'] == $productId) {
                    $productStatus = $product['STATUS'];
                    break;
                }
            }
        }

        if(empty($productStatus)) {
            if($productIsRemoved == 'N') {
                if($productInStock == 'Y') {
                    $productStatus = 'Есть в наличии';
                } else {
                    $productStatus = 'Нет в наличии';
                }
            } else {
                $productStatus = 'Снят с производства';
            }
        }

        /* Получаем минимальную дилерскую цену для товара (НАЧАЛО) */

        $arArticles = [];
        $inStockProducer = [];
        $dealerPrice = '-';
        $minPriceAndCount = [];
        $arPricesOld = array();
        $arPricesNew = array();

        $resProp = CIBlockElement::GetProperty($iblockId, $productId);
        while ($obProp = $resProp->GetNext()) {
            if((strripos($obProp['CODE'], 'ARTICLE_') !== false) && !empty($obProp['VALUE'])) {
                $arArticles[] = $obProp['CODE'];
            }
        }

        if(!empty($arArticles)) {
            $res = CIBlockElement::GetProperty($iblockId, $productId, 'ID', 'DESC', array('ID' => 1391));

            while ($ob = $res->GetNext()) {
                $issetArticle = false;

                foreach ($arArticles as $article) {
                    if(strripos($article, $ob['VALUE_XML_ID']) !== false) {
                        $issetArticle = true;

                        break;
                    }
                }

                if($issetArticle) {
                    $inStockProducer[] = array(
                        'ID' => $ob['VALUE'],
                        'VALUE_XML_ID' => $ob['VALUE_XML_ID'],
                    );
                }
            }
        }

        if(!empty($inStockProducer)) {
            $arFilter = array("PRODUCT_ID" => $productId, '!CATALOG_GROUP_ID' => 1, '!PRICE' => array(false, 0));

            $arSelectFields = array("ID", "CATALOG_GROUP_NAME", "CATALOG_GROUP_ID", "PRICE", "CURRENCY");
            $rsPrice = CPrice::GetListEx(array(), $arFilter, false, false, $arSelectFields);

            while ($dataPrice = $rsPrice->Fetch()) {
                $arPricesOld[$dataPrice['ID']] = $dataPrice;
            }

            if(!empty($arPricesOld)) {
                if($cbr->load()) {
                    foreach ($arPricesOld as $priceId => $dataPrice) {
                        if ($dataPrice['CURRENCY'] != 'RUB') {
                            if(is_array($cbr)) {
                                $cbrExRate = isset($cbr[$dataPrice['CURRENCY']]) ? $cbr[$dataPrice['CURRENCY']] : 0;
                            } else {
                                $cbrExRate = $cbr->get($dataPrice['CURRENCY']);
                            }

                            $dataPrice['PRICE'] = $dataPrice['PRICE'] * $cbrExRate;
                            $dataPrice['CURRENCY'] = 'RUB';

                            $arPricesNew[$priceId] = $dataPrice;
                        } else {
                            $arPricesNew[$priceId] = $dataPrice;
                        }
                    }
                }

                if(!empty($arPricesNew)) {
                    foreach ($arPricesNew as $key => $price) {
                        $dbPriceType = CCatalogGroup::GetList(array(), array('ID' => $price['CATALOG_GROUP_ID']), false, false, array('ID', 'NAME'));

                        if ($arPriceType = $dbPriceType->Fetch()) {
                            if (!empty($arPriceType['NAME'])) {
                                $priceCode = $arPriceType['NAME'];
                                $partsName = explode('__', $priceCode);
                                $vendor = $partsName[0];

                                if (array_search(strtolower($vendor), array_map('strtolower', array_column($inStockProducer, 'VALUE_XML_ID'))) === false) {
                                    unset($arPricesNew[$key]);
                                } else {
                                    $arPricesNew[$key]['PRICE_CODE'] = $priceCode;
                                }
                            } else {
                                unset($arPricesNew[$key]);
                            }
                        } else {
                            unset($arPricesNew[$key]);
                        }
                    }
                }

                if(!empty($arPricesNew)) {
                    $arPricesNewTemp = $arPricesNew;
                    $arPricesNew = array();

                    foreach ($arPricesNewTemp as $keyTemp=>$priceTemp) {
                        if(strripos($priceTemp['PRICE_CODE'], '_rub') === false) {
                            $arPricesNew[$keyTemp] = $priceTemp;
                        }
                    }

                    foreach ($arPricesNewTemp as $keyTemp=>$priceTemp) {
                        if(empty($arPricesNew)) {
                            $arPricesNew[$keyTemp] = $priceTemp;
                        } else {
                            $issetInArPricesNew = false;

                            foreach ($arPricesNew as $keyNew=>$priceNew) {
                                if(strripos($priceTemp['PRICE_CODE'], $priceNew['PRICE_CODE']) !== false) {
                                    $issetInArPricesNew = true;
                                    break;
                                }
                            }

                            if($issetInArPricesNew === false) {
                                $arPricesNew[$keyTemp] = $priceTemp;
                            }
                        }
                    }
                }

                if(!empty($arPricesNew)) {
                    $minPriceAndCount = $parse->getMinPriceAndCount($productId, $arPricesNew);
                }

                if(isset($minPriceAndCount['PRICE'])) {
                    $dealerPrice = CCurrencyLang::CurrencyFormat($minPriceAndCount['PRICE'], 'RUB');
                }
            }
        }

        /* Получаем минимальную дилерскую цену для товара (КОНЕЦ) */

        $dataItem = array(
            'ID' => $basketItem->getField('ID'),
            'PRODUCT_ID' => $productId,
            'NAME' => $prodName,
            'NAME_WITH_URL' => $prodNameWithURL,
            'IMAGE' => $img,
            'CAN_BUY' => $basketItem->canBuy(),
            'DISCOUNT' => $basketItem->getDiscountPrice() * 100,
            'PRICE' => $basketItem->getPrice(),
            'DEALER_PRICE' => $dealerPrice,
            'TOTAL_PRICE' => $basketItem->getFinalPrice(),
            'CURRENCY' => $basketItem->getCurrency(),
            'QUANTITY' => $basketItem->getQuantity(),
            'MEASURE_NAME' => $basketItem->getField('MEASURE_NAME'),
            'QUANTITY' => $basketItem->getQuantity(),
            'STATUS' => $productStatus,
        );

        $arResult['ITEMS'][] = $dataItem;
    }

    $arResult['TOTAL_SUM'] = $basket->getPrice();
    $arResult['ID'] = isset($cpId) ? $cpId : '';
    $arResult['ADDITIONAL_INFORMATION'] = isset($additionalInf) ? $additionalInf : '';

    if(isset($numberCP) && !empty($numberCP)) {
        $arResult['NUMBER'] = $numberCP;
    } else {
        $rsCP = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => $iblockCPId), array('ID', 'NAME', 'PROPERTY_NUMBER'));

        if ($arCP = $rsCP->GetNext()) {
            if(isset($arCP['PROPERTY_NUMBER_VALUE'])
                && !empty($arCP['PROPERTY_NUMBER_VALUE'])) {
                $numberCP = $arCP['PROPERTY_NUMBER_VALUE'] + 1;
            } else {
                $numberCP = 100;
            }
        } else {
            $numberCP = 100;
        }

        $arResult['NUMBER'] = $numberCP;
    }

    $arResult['CREATED_DATE'] = isset($dateCP) ? $dateCP : date('j.m.Y');
} else {
    $arResult = [];
}

$listAllSavedCP = array();

$rsCP = CIBlockElement::GetList(array('ID' => 'ASC'), array('IBLOCK_ID' => $iblockCPId), array('ID', 'NAME', 'CREATED_DATE'));

while ($arCP = $rsCP->GetNext()) {
    $arCP['CREATED_DATE'] = FormatDate("d.m.Y", MakeTimeStamp($arCP['CREATED_DATE']));

    $listAllSavedCP[] = $arCP;
}

$arResult['LIST_ALL_SAVED_CP'] = $listAllSavedCP;

$this->IncludeComponentTemplate();