<?
$postData = $_POST;

if(!isset($_POST['prodData']) or empty($_POST['prodData'])) {
    $result = array(
        'status' => 'error',
        'message' => 'Не удалось сохранить КП. Пришли неправильные данные.',
    );

    echo json_encode($result);
    exit();
}

$iblockId = 37;

$subDataNewCP = array();
$arProds = array();
$arPrices = array();
$arQuantities = array();
$arStatuses = array();

if(isset($_POST['number-cp'])) {
    $subDataNewCP['NUMBER'] = $_POST['number-cp'];
}

if(isset($_POST['additional_inf'])) {
    $subDataNewCP['ADDITIONAL_INFORMATION'] = $_POST['additional_inf'];
}

if(isset($_POST['total_sum'])) {
    $subDataNewCP['TOTAL_SUM_CP'] = $_POST['total_sum'];
}

foreach ($_POST['prodData'] as $prodId => $product) {
    $prodId = str_replace("'", "", $prodId);
    $arProds[] = $prodId;
    $arPrices[] = trim(preg_replace('/[^\d.,]/', '', $product['price']), ',.');
    $arQuantities[] = $product['quantity'];
    $arStatuses[] = $product['status'];
}

$subDataNewCP['PRODUCTS'] = $arProds;
$subDataNewCP['PRODUCTS_PRICES'] = $arPrices;
$subDataNewCP['PRODUCTS_QUANTITIES'] = $arQuantities;
$subDataNewCP['PRODUCTS_STATUSES'] = $arStatuses;

$el = new CIBlockElement;

if(isset($_POST['id'])
    && !empty($_POST['id'])) {
    $id = $_POST['id'];

    $countElements = CIBlockElement::GetList(array('ID' => 'ASC'), array('ID' => $id), array());

    if((int) $countElements < 1) {
        $result = array(
            'status' => 'error',
            'message' => 'Не удалось обновить КП. Указанный КП не найден.',
        );

        echo json_encode($result);
        exit();
    }

    $dataNewCP = array(
        "MODIFIED_BY"    => 1,
        "IBLOCK_ID"      => $iblockId,
        "PROPERTY_VALUES"=> $subDataNewCP,
        "ACTIVE"         => "Y",
    );

    if($el->Update($id, $dataNewCP)) {
        $result = array(
            'status' => 'ok',
            'message' => 'КП успешно сохранено.',
        );

        echo json_encode($result);
        exit();
    } else {
        $result = array(
            'status' => 'error',
            'message' => 'Не удалось сохранить КП',
        );

        echo json_encode($result);
        exit();
    }
} else {
    $dataNewCP = array(
        "MODIFIED_BY"    => 1,
        "IBLOCK_ID"      => $iblockId,
        "PROPERTY_VALUES"=> $subDataNewCP,
        "NAME"           => (isset($_POST['number-cp']) && !empty($_POST['number-cp'])) ? 'КП №' . $_POST['number-cp'] : 'КП от ' . date("d.m.Y"),
        "ACTIVE"         => "Y",
    );

    if($newCPId = $el->Add($dataNewCP)) {
        $result = array(
            'status' => 'ok',
            'message' => 'КП успешно сохранено.',
            'new_cp_id' => $newCPId,
        );

        echo json_encode($result);
        exit();
    } else {
        $result = array(
            'status' => 'error',
            'message' => 'Не удалось сохранить КП',
        );

        echo json_encode($result);
        exit();
    }
}

?>