<?

namespace Local\SoapImport;

use Bitrix\Main\Application;
use Bitrix\Sale;
use CCatalogGroup;
use CIBlockElement;
use CSaleUser;
use Bitrix\Main\Context;
use Bitrix\Currency\CurrencyManager;
use Exception;
use soapImport;

$root = Application::getDocumentRoot() . Application::getPersonalRoot();
include_once($root . '/php_interface/include/Logger.php');
include_once($root . '/php_interface/soap_import.php');

class SoapImportOrders extends soapImport {
    private $listChangedWebOrders = [];
    private $cnt_total = 0;
    private $cnt_updated = 0;
    private $siteId;
    private $currencyCode;
    private $arPricesGroup = [];
    private $statusesOrder = array(
        1 => array(
            'CODE' => 'WC',
            'TITLE_WEB' => 'Ждет подтверждения',
            'TITLE_API' => 'Ждет подтверждения',
        ),
        2 => array(
            'CODE' => 'AP',
            'TITLE_WEB' => 'Ждет оплаты',
            'TITLE_API' => 'Ждет оплаты',
        ),
        3 => array(
            'CODE' => 'P',
            'TITLE_WEB' => 'Передан на сборку',
            'TITLE_API' => 'Передан на сборку',
        ),
        4 => array(
            'CODE' => 'RS',
            'TITLE_WEB' => 'Готов к отгрузке',
            'TITLE_API' => 'Готов к отгрузке',
        ),
        5 => array(
            'CODE' => 'S',
            'TITLE_WEB' => 'Отгружен',
            'TITLE_API' => 'Отгружен',
        ),
        6 => array(
            'CODE' => 'F',
            'TITLE_WEB' => 'Выполнен',
            'TITLE_API' => 'Завершен',
        ),
        7 => array(
            'CODE' => 'C',
            'TITLE_WEB' => 'Отменен',
            'TITLE_API' => 'Отменен',
        ),
    );

    private function init() {
        $this->siteId = Context::getCurrent()->getSite();
        $this->currencyCode = CurrencyManager::getBaseCurrency();
        $this->listChangedWebOrders = $this->_client->GetChangedDpfWebOrders()->GetChangedDpfWebOrdersResult->DpfWebOrderView;

        //типы цен
        $dbPriceType = CCatalogGroup::GetList(["SORT" => "ASC"]);
        while ($arPriceType = $dbPriceType->Fetch()) {
            $this->arPricesGroup[$arPriceType["XML_ID"]] = array(
                'ID' => $arPriceType["ID"],
                'NAME' => $arPriceType["NAME"],
                'NAME_LANG' => $arPriceType["NAME_LANG"],
            );
        }

        if(empty($this->arPricesGroup)) {
            $this->log('Не удалось получить список групп цен');
            $this->logger->addRow([-1, ' ', 'Не удалось получить список групп цен']);
            $this->error_log($this->_import_id . ': Не удалось получить список групп цен');

            parent::rerunImport($this->_import_id);
        }

        /* УДАЛИТЬ */

        /*$test = $this->_client->GetDpfWebOrder(array('siteWebOrderID' => 39251))->GetDpfWebOrderResult;

        echo "<pre>";
        print_r($test);
        echo "</pre>";
        exit;*/

        /*$this->importChangesOrder($test);

        echo "<pre>";
        print_r($test);
        echo "</pre>";
        exit;*/

        /* УДАЛИТЬ */
    }

    public function importChangesOrders() {
        $this->_import_id = 'importChangesOrders';
        $this->log('Начинаю импорт изменений заказов...');
        $this->logger->setHeader(['ID заказа', ' ', 'Результат']);

        try {
            $this->init();

            if(!empty($this->listChangedWebOrders)) {
                if(!is_array($this->listChangedWebOrders)) {
                    $tmpListChangedWebOrders = $this->listChangedWebOrders;
                    $this->listChangedWebOrders = array();
                    $this->listChangedWebOrders[] = $tmpListChangedWebOrders;
                }

                foreach ($this->listChangedWebOrders as $changedWebOrder) {
					$this->importChangesOrder($changedWebOrder);
                }
            }

            $msg = 'Импорт изменений заказов завершен ('
                . 'всего: ' . $this->cnt_total . '; '
                . 'успешно обновлено: ' . $this->cnt_updated . '; '
                . 'неуспешно: ' . ($this->cnt_total - $this->cnt_updated) . ').';
            $this->log($msg . PHP_EOL);
            $this->logger->setFooter($msg);
            //$this->sendEmails($this->_import_id);
        } catch (Exception $e) {
            $this->log('Произошла неизвестная ошибка при импорте изменений заказа: ' . print_r($e->getMessage(), 1));
            $this->logger->addRow([-1, ' ', 'Произошла неизвестная ошибка при импорте изменений заказа: ' . print_r($e->getMessage(), 1)]);
            $this->error_log($this->_import_id . ': Произошла неизвестная ошибка при импорте изменений заказа: ' . print_r($e->getMessage(), 1));

            parent::rerunImport($this->_import_id);
        }
    }

    public function importChangesOrder($changedWebOrder) {
        $this->cnt_total++;

        $orderId = $changedWebOrder->SiteWebOrderID;

        $order = Sale\Order::load($orderId);

        if(empty($order)) {
            $this->log('Ошибка: Не удалось найти заказ №' . $orderId);
            $this->error_log($this->_import_id . ': Не удалось найти заказ №' . $orderId);
            $this->logger->addRow([$orderId, ' ', 'Ошибка: Не удалось найти заказ №' . $orderId]);

            return false;
        }

        if(isset($changedWebOrder->Status->Value)
            && !empty($changedWebOrder->Status->Value) && isset($this->statusesOrder[$changedWebOrder->Status->Value])) {
            $order->setField('STATUS_ID', $this->statusesOrder[$changedWebOrder->Status->Value]['CODE']);
        }

        if(isset($changedWebOrder->PaymentState)
            && !empty($changedWebOrder->PaymentState)) {
            $propertyCollection = $order->getPropertyCollection();

            foreach ($propertyCollection as $prop) {
                if($prop->getField('CODE') == 'PAYMENT_STATUS') {
                    $prop->setField('VALUE', $changedWebOrder->PaymentState);
                }
            }
        }

        if(isset($changedWebOrder->Comment)
            && !empty($changedWebOrder->Comment)) {
            $order->setField('USER_DESCRIPTION', $changedWebOrder->Comment);
        }

        $basket = $order->getBasket();

        if(isset($changedWebOrder->Items->DpfWebOrderItemView)) {
            if(!is_array($changedWebOrder->Items->DpfWebOrderItemView)) {
                $tmpDpfWebOrderItemView = $changedWebOrder->Items->DpfWebOrderItemView;
                $changedWebOrder->Items->DpfWebOrderItemView = array();
                $changedWebOrder->Items->DpfWebOrderItemView[] = $tmpDpfWebOrderItemView;
            }

            $processedProdsInBasket = array();

            foreach ($changedWebOrder->Items->DpfWebOrderItemView as $newItemAPI) {
                $dbElement = CIBlockElement::GetList([], ["IBLOCK_ID" => self::IBLOCK_ID, "EXTERNAL_ID" => $newItemAPI->CargoID],
                    false, ["nTopCount" => 1], ["ID", "NAME", "DETAIL_PAGE_URL", 'XML_ID']);
                if($arElement = $dbElement->Fetch()) {
                    $isNew = true;

                    if(!empty($basket)) {
                        foreach ($basket as $basketItem) {
                            $basketRowId = $basketItem->getId();
                            $productId = $basketItem->getProductId();

                            if($productId == $arElement['ID']) {
                                $priceData = $this->getPriceData($changedWebOrder->GlobalPlaceID, $newItemAPI->DiscountID);

                                $newItemData = array(
                                    'NAME' => $arElement['NAME'],
                                    'QUANTITY' => $newItemAPI->Quantity,
                                    'PRODUCT_XML_ID' => $arElement['XML_ID'],
                                    'PRICE' => $newItemAPI->Price,
                                    'BASE_PRICE' => $newItemAPI->Price,
                                );

                                if(isset($priceData)) {
                                    $newItemData['PRICE_TYPE_ID'] = $priceData['ID'];
                                    $newItemData['NOTES'] = $priceData['NAME_LANG'];
                                    $newItemData['CUSTOM_PRICE'] = 'N';
                                } else {
                                    $newItemData['CUSTOM_PRICE'] = 'Y';
                                }

                                $basketItem->setFields($newItemData);

                                if(!in_array($arElement['ID'], $processedProdsInBasket)) {
                                    $processedProdsInBasket[] = $arElement['ID'];
                                }

                                $isNew = false;
                            }
                        }
                    }

                    if($isNew) {
                        $item = $basket->createItem('catalog', $arElement['ID']);

                        $priceData = $this->getPriceData($changedWebOrder->GlobalPlaceID, $newItemAPI->DiscountID);

                        $newItemData = array(
                            'NAME' => $arElement['NAME'],
                            'QUANTITY' => $newItemAPI->Quantity,
                            'CURRENCY' => $this->currencyCode,
                            'LID' => $this->siteId,
                            'PRODUCT_PROVIDER_CLASS' => '\Bitrix\Catalog\Product\CatalogProvider',
                            'PRODUCT_XML_ID' => $arElement['XML_ID'],
                            'PRICE' => $newItemAPI->Price,
                            'BASE_PRICE' => $newItemAPI->Price,
                        );

                        if(isset($priceData)) {
                            $newItemData['PRICE_TYPE_ID'] = $priceData['ID'];
                            $newItemData['NOTES'] = $priceData['NAME_LANG'];
                            $newItemData['CUSTOM_PRICE'] = 'N';
                        } else {
                            $newItemData['CUSTOM_PRICE'] = 'Y';
                        }

                        $item->setFields($newItemData);

                        if(!in_array($arElement['ID'], $processedProdsInBasket)) {
                            $processedProdsInBasket[] = $arElement['ID'];
                        }
                    }
                }
            }

            if(!empty($basket) && !empty($processedProdsInBasket)) {
                foreach ($basket as $basketItem) {
                    $productId = $basketItem->getProductId();

                    if(!in_array($productId, $processedProdsInBasket)) {
                        $basketItem->delete();
                    }
                }
            }
        }

        $order->refreshData();
		$order->doFinalAction();
        $resSaveOrder = $order->save();

        if ($resSaveOrder->isSuccess()) {
            $this->cnt_updated++;

            $this->_client->ConfirmChangedWebOrderReception(array('siteWebOrderID' => $orderId));
        } else {
            $this->log('Ошибка: При изменении заказа №' . $orderId . ' произошла неизвестная ошибка. ' . print_r($resSaveOrder->getErrorMessages(), 1));
            $this->error_log($this->_import_id . ': При изменении заказа №' . $orderId . ' произошла неизвестная ошибка. ' . print_r($resSaveOrder->getErrorMessages(), 1));
            $this->logger->addRow([$orderId, ' ', 'Ошибка: При изменении заказа произошла неизвестная ошибка. ' . print_r($resSaveOrder->getErrorMessages(), 1)]);

            return false;
        }
    }

    private function getPriceData($territoryID, $discountID) {
        return $this->arPricesGroup[md5($territoryID.$discountID)];
    }

    public function getDataSelectedOrderFromAPI($orderId) {
        $dataOrder = $this->_client->GetDpfWebOrder(array('siteWebOrderID' => $orderId))->GetDpfWebOrderResult;

        return $dataOrder;
    }
}