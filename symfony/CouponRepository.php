<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Coupon;
use App\Entity\User;
use App\Geocoding\DistanceUtility;
use App\Model\Location;
use App\Repository\Filter\CouponFilter;
use App\Repository\Filter\CouponLocationFilter;
use App\Repository\Subquery\UserSubquery;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\OrderBy;
use Geokit\LatLng;
use Geokit\Math;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Coupon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coupon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coupon[]    findAll()
 * @method Coupon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CouponRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Coupon::class);
    }

    public function countAllWithCategory(CouponFilter $couponFilter)
    {
        $qb = $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->innerJoin('c.categories', 'cat')
            ->innerJoin('c.redeemRules', 'rr')
            ->innerJoin('c.user', 'u');

        $constraints = [];
        $constraintsFilter = [];
        $constraintsFilterState = [];

        if ($couponFilter->getUser() !== null) {
            $qb->setParameter('user', $couponFilter->getUser());
            $constraints[] = $qb->expr()->eq('c.user', ':user');
        }

        if ($couponFilter->getFilterState() !== null) {
            $filterState = $couponFilter->getFilterState();

            foreach (explode(',', $filterState) as $key => $filter) {
                $constraintsFilterState[] = $qb->expr()->eq('c.state', ':filterState' . $key);
                $qb->setParameter('filterState' . $key, $filter);
            }
        }

        if (!empty($constraintsFilterState)) {
            $constraints[] = $qb->expr()->orX(...$constraintsFilterState);
        }

        if ($couponFilter->getFilter() !== null) {
            $constraintsFilter[] = $qb->expr()->like('u.company', ':filter');
            $constraintsFilter[] = $qb->expr()->like('c.title', ':filter');
            $constraintsFilter[] = $qb->expr()->like('c.validFrom', ':filter');
            $constraintsFilter[] = $qb->expr()->like('c.validUntil', ':filter');
            $constraintsFilter[] = $qb->expr()->like('c.state', ':filter');
            $qb->setParameter('filter', '%' . $couponFilter->getFilter() . '%');
        }

        if (!empty($constraintsFilter)) {
            $constraints[] = $qb->expr()->orX(...$constraintsFilter);
        }

        if (!empty($constraints)) {
            $qb->where($qb->expr()->andX(...$constraints));
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllWithCategory(CouponFilter $couponFilter)
    {
        $qb = $this->createQueryBuilder('c')
            ->addSelect('rr', 'cat')
            ->innerJoin('c.categories', 'cat')
            ->innerJoin('c.redeemRules', 'rr')
            ->innerJoin('c.user', 'u');

        $constraints = [];
        $constraintsFilter = [];
        $constraintsFilterState = [];

        if ($couponFilter->getUser() !== null) {
            $qb->setParameter('user', $couponFilter->getUser());
            $constraints[] = $qb->expr()->eq('c.user', ':user');
        }

        if ($couponFilter->getFilterState() !== null) {
            $filterState = $couponFilter->getFilterState();

            foreach (explode(',', $filterState) as $key => $filter) {
                $constraintsFilterState[] = $qb->expr()->eq('c.state', ':filterState' . $key);
                $qb->setParameter('filterState' . $key, $filter);
            }
        }

        if (!empty($constraintsFilterState)) {
            $constraints[] = $qb->expr()->orX(...$constraintsFilterState);
        }

        if ($couponFilter->getFilter() !== null) {
            $constraintsFilter[] = $qb->expr()->like('c.id', ':filter');
            $constraintsFilter[] = $qb->expr()->like('u.company', ':filter');
            $constraintsFilter[] = $qb->expr()->like('c.title', ':filter');
            $constraintsFilter[] = $qb->expr()->like('c.validFrom', ':filter');
            $constraintsFilter[] = $qb->expr()->like('c.validUntil', ':filter');
            $constraintsFilter[] = $qb->expr()->like('c.state', ':filter');
            $qb->setParameter('filter', '%' . $couponFilter->getFilter() . '%');
        }

        if (!empty($constraintsFilter)) {
            $constraints[] = $qb->expr()->orX(...$constraintsFilter);
        }

        if (!empty($constraints)) {
            $qb->where($qb->expr()->andX(...$constraints));
        }

        if ($couponFilter->getSort() !== null) {
            if ($couponFilter->getSort() == 'user') {
                $qb->orderBy('u.company', $couponFilter->getSortType());
            } elseif ($couponFilter->getSort() == 'title') {
                $qb->orderBy('c.title', $couponFilter->getSortType());
            } elseif ($couponFilter->getSort() == 'validFrom') {
                $qb->orderBy('c.validFrom', $couponFilter->getSortType());
            } elseif ($couponFilter->getSort() == 'validUntil') {
                $qb->orderBy('c.validUntil', $couponFilter->getSortType());
            } elseif ($couponFilter->getSort() == 'state') {
                $qb->orderBy('c.state', $couponFilter->getSortType());
            } elseif ($couponFilter->getSort() == 'id') {
                $qb->orderBy('c.id', $couponFilter->getSortType());
            }
        }

        if ($couponFilter->getPage() !== null) {
            $qb->setFirstResult(($couponFilter->getPage() - 1) * $couponFilter->getRowsPerPage());
        }

        if ($couponFilter->getRowsPerPage() !== null) {
            $qb->setMaxResults($couponFilter->getRowsPerPage());
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findActiveById(array $id): array
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->addSelect('u', 'rr', 'cat')
            ->innerJoin('c.user', 'u')
            ->innerJoin('c.redeemRules', 'rr')
            ->innerJoin('c.categories', 'cat')
            ->setParameter('state', Coupon::STATE_PUBLISHED)
            ->setParameter('now', new \DateTimeImmutable())
            ->setParameter('id', $id)
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('c.state', ':state'),
                    $qb->expr()->lte('c.validFrom', ':now'),
                    $qb->expr()->gte('c.validUntil', ':now'),
                    $qb->expr()->in('c.id', ':id'),
                    UserSubquery::createActiveCustomerConstraint($qb, 'c.user')
                )
            )
            ->getQuery()
            ->getResult();
    }

    public function findActiveByCouponLocationFilter(CouponLocationFilter $couponLocationFilter, OrderBy $orderBy = null, $limit = null)
    {
        $qb = $this->createQueryBuilder('c');

        $lat = $couponLocationFilter->getLocationCenter()->getLat();
        $lng = $couponLocationFilter->getLocationCenter()->getLng();

        // Calculate distance to coupon with pythagoras (not really accurate, but good enough for our use case)
        $distanceSelect = 'SQRT( (111300 * (a1.latitude - ' . $lat . ')) * (111300 * (a1.latitude - ' . $lat . ')) + (71500 * (a1.longitude - ' . $lng . ')) * (71500 * (a1.longitude - ' . $lng . ')) )';
        // Same for stores subquery
        $distanceSelect2 = 'SQRT( (111300 * (a2.latitude - ' . $lat . ')) * (111300 * (a2.latitude - ' . $lat . ')) + (71500 * (a2.longitude - ' . $lng . ')) * (71500 * (a2.longitude - ' . $lng . ')) )';

        // Determine latitude/longitude bounding box to reduce database load
        $bounds = (new Math())->expand(new LatLng($lat, $lng), $couponLocationFilter->getLocationRadius());

        $qb
            ->setParameter('minLatitude', $bounds->getSouthWest()->getLatitude())
            ->setParameter('minLongitude', $bounds->getSouthWest()->getLongitude())
            ->setParameter('maxLatitude', $bounds->getNorthEast()->getLatitude())
            ->setParameter('maxLongitude', $bounds->getNorthEast()->getLongitude())
            ->setParameter('state', $couponLocationFilter->getState())
            ->setParameter('now', new DateTimeImmutable());

        $constraints = [
            $qb->expr()->eq('c.state', ':state'),
            $qb->expr()->lte('c.validFrom', ':now'),
            $qb->expr()->gte('c.validUntil', ':now'),
            $qb->expr()->gte('a1.latitude', ':minLatitude'),
            $qb->expr()->gte('a1.longitude', ':minLongitude'),
            $qb->expr()->lte('a1.latitude', ':maxLatitude'),
            $qb->expr()->lte('a1.longitude', ':maxLongitude'),
            UserSubquery::createActiveCustomerConstraint($qb, 'c.user'),
        ];

        if ($couponLocationFilter->isLatestCouponsOnly()) {
            $qb->setParameter('minPublicationDate', (new DateTimeImmutable())->modify('-7 days'));
            $constraints[] = $qb->expr()->gte('c.publicationDate', ':minPublicationDate');
        }

        if (!empty($couponLocationFilter->getCategories())) {
            $qb->leftJoin('c.categories', 'cat');
            $qb->setParameter('categories', $couponLocationFilter->getCategories());
            $constraints[] = $qb->expr()->in('cat.id', ':categories');
        }

        if (null === $orderBy) {
            $orderBy = new OrderBy();
            $orderBy->add('distance', 'ASC');
            // Avoid random order of same company coupons (they have all the same distance)
            $orderBy->add('c.publicationDate', 'DESC');
        }

        // Join condition which only joins the closes store, so we can order by the distance to this store and ignore all other stores
        $closestStoreJoinCondition = 's = FIRST(SELECT s2.id FROM App\Entity\Store s2 JOIN App\Entity\Address a2 WITH a2.id = s2.address WHERE s2.user = u.id AND (a2.longitude IS NOT NULL) AND (a2.latitude IS NOT NULL) ORDER BY ' . $distanceSelect2 . ' ASC)';

        $result = $qb
            ->select('c.id', $distanceSelect . ' AS distance', 's.id AS closestStoreId')
            ->leftJoin('c.user', 'u')
            //Find the store with the shortest distance to $locationCenter
            ->leftJoin('u.stores', 's', 'WITH', $closestStoreJoinCondition)
            ->leftJoin('s.address', 'a1')
            ->where($qb->expr()->andX(...$constraints))
            ->setMaxResults($limit)
            ->orderBy($orderBy)
            ->getQuery()
            ->getArrayResult();

        foreach ($result as &$item) {
            $item['distance'] = (int) $item['distance'];
        }

        return $result;
    }

    public function findActiveLatestByLocation(Location $location)
    {
        $couponLocationFilter = new CouponLocationFilter(Coupon::STATE_PUBLISHED, $location, 1000000, [], false);

        return $this->findActiveByCouponLocationFilter($couponLocationFilter, new OrderBy('c.publicationDate', 'DESC'), 15);
    }

    public function findActiveOrderByDistance(Location $location)
    {
        /** @var Coupon[] $collection */
        $collection = $this->findActive();
        usort($collection, function($a, $b) use ($location) {
            return $this->sortByDistance($location, $a, $b);
        });

        return $collection;
    }

    public function findActive(int $limit = null, array $sort = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->setParameter('state', Coupon::STATE_PUBLISHED);
        $qb->setParameter(':now', new \DateTimeImmutable());

        $constraints = $qb->expr()->andX(
            $qb->expr()->eq('coupon.state', ':state'),
            $qb->expr()->lte('coupon.validFrom', ':now'),
            $qb->expr()->gte('coupon.validUntil', ':now')
        );

        $orderBy = new OrderBy();
        if (empty($sort)) {
            $orderBy->add('coupon.modified', 'DESC');
        } else {
            foreach ($sort as $field) {
                $order = 'ASC';
                if (0 === strpos($field, '-')) {
                    $field = substr($field, 1);
                    $order = 'DESC';
                }
                $orderBy->add('coupon.' . $field, $order);
            }
        }

        $qb
            ->select('coupon, user,redeemRules,categories')
            ->from(Coupon::class, 'coupon')
            ->leftJoin('coupon.user', 'user')
            ->leftJoin('coupon.redeemRules', 'redeemRules')
            ->leftJoin('coupon.categories', 'categories')
            ->where($constraints)
            ->orderBy($orderBy)
            ->setMaxResults($limit);

        return $qb
            ->getQuery()
            ->getResult();
    }

    /**
     * Find active coupons of $owner.
     * Respects state, validFrom, validUntil and membership state
     *
     * @param User $owner
     *
     * @return mixed
     */
    public function findActiveByOwner(User $owner)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb
            ->select('coupon, user,redeemRules,categories')
            ->from(Coupon::class, 'coupon')
            ->leftJoin('coupon.user', 'user')
            ->leftJoin('coupon.redeemRules', 'redeemRules')
            ->leftJoin('coupon.categories', 'categories')
            ->setParameter('state', Coupon::STATE_PUBLISHED)
            ->setParameter('now', new \DateTimeImmutable())
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('coupon.state', ':state'),
                    $qb->expr()->eq('coupon.user', $owner->getId()),
                    $qb->expr()->lte('coupon.validFrom', ':now'),
                    $qb->expr()->gte('coupon.validUntil', ':now')
                )
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * Find coupons by owner and state. Ignores validFrom, validUntil and membership state
     *
     * @param User   $owner
     * @param string $state
     *
     * @return mixed
     */
    public function findByOwnerAndState(User $owner, string $state)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb
            ->select('coupon')
            ->from(Coupon::class, 'coupon')
            ->setParameter('state', $state)
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('coupon.state', ':state'),
                    $qb->expr()->eq('coupon.user', $owner->getId())
                )
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     *
     * @return Coupon[]
     */
    public function findActiveWithExpireDateBetween(\DateTimeInterface $start, \DateTimeInterface $end): array
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->setParameter('state', Coupon::STATE_PUBLISHED)
            ->setParameter(':start', $start)
            ->setParameter(':end', $end)
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('c.state', ':state'),
                    $qb->expr()->between('c.validUntil', ':start', ':end'),
                    UserSubquery::createActiveCustomerConstraint($qb, 'c.user')
                )
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * Count coupons which are in review state and are owned by an active customer
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return int
     */
    public function countInReview(): int
    {
        $qb = $this->createQueryBuilder('c');

        $count = $qb
            ->select('count(c.id)')
            ->setParameter('state', Coupon::STATE_REVIEW)
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('c.state', ':state'),
                    UserSubquery::createActiveCustomerConstraint($qb, 'c.user')
                )
            )
            ->getQuery()
            ->getSingleScalarResult();

        return (int) $count;
    }

    /**
     * Count the number of coupons which were active inside the date range.
     * It's enough, if the coupon was active for one day inside the range!
     *
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return mixed
     */
    public function countActiveByDateRange(\DateTimeInterface $start, \DateTimeInterface $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->setParameter('state', Coupon::STATE_PUBLISHED);
        $qb->setParameter(':start', $start);
        $qb->setParameter(':end', $end);

        return $qb
            ->select('count(c.id)')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('c.state', ':state'),
                    $qb->expr()->orX(
                        $qb->expr()->andX(
                        // Coupon was active the full date range
                            $qb->expr()->lte('c.validFrom', ':start'),
                            $qb->expr()->gte('c.validUntil', ':end')
                        ),
                        // Coupon was active for a single moment inside date range (validFrom is inside range)
                        $qb->expr()->between('c.validFrom', ':start', ':end'),
                        // Coupon was active for a single moment inside date range (validUntil is inside range)
                        $qb->expr()->between('c.validUntil', ':start', ':end')
                    )
                )
            )
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function sortByDistance(Location $location, Coupon $a, Coupon $b)
    {
        try {
            $p1 = new Location($a->getUser()->getLat(), $a->getUser()->getLng());
            $p1Distance = DistanceUtility::calcKilometres($p1, $location);
        } catch (\Throwable $e) {
            return -1;
        }

        try {
            $p2 = new Location($b->getUser()->getLat(), $b->getUser()->getLng());
            $p2Distance = DistanceUtility::calcKilometres($p2, $location);
        } catch (\Throwable $e) {
            return 1;
        }

        return $p1Distance > $p2Distance ? -1 : 1;
    }
}
