<?php

declare(strict_types=1);

namespace App\Service\Coupon;

use App\Entity\Coupon;
use App\Messaging\Mail\MailerInterface;
use App\Repository\CouponRedeemRepository;
use App\Repository\CouponRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CouponService
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var CouponRepository
     */
    protected $couponRepository;

    /**
     * @var CouponRedeemRepository
     */
    protected $couponRedeemRepository;

    /**
     * @var MailerInterface
     */
    protected $mailer;

    public function __construct(TranslatorInterface $translator, EntityManagerInterface $entityManager, CouponRepository $couponRepository, MailerInterface $mailer)
    {
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->couponRepository = $couponRepository;
        $this->mailer = $mailer;
    }

    public function enableReview(Coupon $coupon): Coupon
    {
        $this->mailer->sendCouponReviewNotificationToCoupocket($coupon);
        $coupon->setState(Coupon::STATE_REVIEW);
        $this->entityManager->persist($coupon);
        $this->entityManager->flush();

        return $coupon;
    }

    /**
     * @param Coupon $coupon
     * @param string $reason
     *
     * @throws \App\Messaging\Mail\InvalidAttachmentException
     */
    public function rejectReview(Coupon $coupon, string $reason)
    {
        if ($coupon->isStateDraft()) {
            // Already draft, do nothing
            return;
        }

        if (!$coupon->isStateReview()) {
            throw new \InvalidArgumentException("Can't reject coupon which is is in state " . $coupon->getState());
        }

        $coupon->setState(Coupon::STATE_DRAFT);
        $this->entityManager->persist($coupon);
        $this->entityManager->flush();

        $this->mailer->sendHtmlToCustomer(
            $coupon->getUser(),
            $this->translator->trans('customer.mail.couponRejected.subject'),
            nl2br($this->translator->trans('customer.mail.couponRejected.body', ['%title%' => $coupon->getTitle(), '%reason%' => $reason]))
        );
    }

    /**
     * Publish coupon, set publicationDate and inform owner
     *
     * @param Coupon $coupon
     *
     * @throws \InvalidArgumentException
     * @throws \App\Messaging\Mail\InvalidAttachmentException
     */
    public function publish(Coupon $coupon)
    {
        if ($coupon->isStatePublished()) {
            // Already published, do nothing
            return;
        }

        if (!$coupon->isStateReview()) {
            throw new \InvalidArgumentException("Can't publish coupon which is is in state " . $coupon->getState());
        }

        $this->mailer->sendHtmlToCustomer(
            $coupon->getUser(),
            $this->translator->trans('customer.mail.couponPublished.subject'),
            nl2br($this->translator->trans('customer.mail.couponPublished.body', ['%title%' => $coupon->getTitle()]))
        );

        if (null !== $coupon->getShop()) {
            $this->mailer->sendHtmlToShop(
                $coupon->getShop(),
                $this->translator->trans('customer.mail.couponPublished.subject'),
                nl2br($this->translator->trans('customer.mail.couponPublished.body', ['%title%' => $coupon->getTitle()]))
            );
        }

        $coupon->setState(Coupon::STATE_PUBLISHED);
        $coupon->setPublicationDate(new \DateTimeImmutable());
        $this->entityManager->persist($coupon);
        $this->entityManager->flush();
    }
}
