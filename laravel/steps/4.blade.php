<h2 class="userPage__title h3">Заявка успешно размещена</h2>
<div class="orderComplete">
    <div class="orderComplete__img">
        <img src="/img/svg/Final image.svg">
    </div>
    <div class="orderComplete__text">
        Через <span id="countdown">3 секунды</span> <br>  вы будете перенаправлены на страницу отслеживания заявок
    </div>
</div>
<script type="text/javascript">

    function declOfNum(number, titles) {
        cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    }

    var seconds = 3;

    function countdown() {
        seconds = seconds - 1;
        if (seconds < 0) {
            window.location = "{{ route('org-orders') }}";
        } else {
            document.getElementById("countdown").innerHTML = seconds + ' ' + declOfNum(seconds, ['секунду', 'секунды', 'секунд']);
            window.setTimeout("countdown()", 1000);
        }
    }



    countdown();

</script>
<div class="orderBottom">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            @include('steps.steper')
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
