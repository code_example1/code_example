<h2 class="userPage__title h3">Размещение заявки</h2>
<div class="order">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-6 col-md-5">
                    <div class="previewMak">
                        <div class="previewMak__title">
                            Отображение лицевой стороны листа
                        </div>
                        <div class="previewMak__img">
                            <a href="{{ $thumb['front'] }}" data-fancybox="gallery">
                                <img src="{{ $thumb['front'] }}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-5 offset-md-1">
                    <div class="previewMak">
                        <div class="previewMak__title">
                            Отображение оборотной стороны листа
                        </div>
                        <div class="previewMak__img">
                            <a href="{{ $thumb['back'] }}" data-fancybox="gallery">
                                <img src="{{ $thumb['back'] }}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex align-items-center">
            <div class="orderPlace">
                <div class="orderPlace__title">
                    Параметры и стоимость заказа:
                </div>
                <div class="orderPlace__count">
                    {{ $count.' '.morph($count, 'письмо', 'письма', 'писем') }}
                </div>
                <div class="orderPlace__price">
                    @money($price)
                </div>
                <div class="orderPlace__nds">
                    сумма указана с учетом НДС (20%)
                </div>
                @if($draft->canOrder)
                <div class="orderPlace__btn">
                    <a href="#"  onclick="changePage(4, event)" id="nextPage" class="btn btn-danger">Оплатить и отправить</a>
                </div>
                @else
                    <div class="orderPlace__btn redText text-center">
                        Сумма на вашем счете недостаточна. <a href="{{ route('user-billing') }}">Пополните баланс</a> и попробуйте снова.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="orderBottom">
    <div class="row">
        <div class="col-md-2 order-2 order-md-1 mb-md-0 mb-4">
            <a href="#" onclick="changePage(2, event)" class="orderBottom__back btn btn-primary">НАЗАД</a>
        </div>
        <div class="col-md-8 order-1 order-md-2 mb-md-0 mb-4">
            @include('steps.steper')
        </div>
        <div class="col-md-2 order-3 order-md-3">
        </div>
    </div>
</div>
