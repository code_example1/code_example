<h2 class="userPage__title h3">Оплата заявки</h2>
<div class="order">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                Заявка успешно сформирована, для отправки необходимо произвести оплату.
                <div class="mt-4">
                    <form class="a-form" action="{{ action('PayController@createFiz', $order) }}" method="post">
                        @csrf
                        <button class="btn btn-lg btn-danger w-75" type="submit">Оплатить заявку</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="orderBottom">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            @include('steps.steper')
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
