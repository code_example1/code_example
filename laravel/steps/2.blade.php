<h2 class="userPage__title h3">Утверждение макета заявки</h2>
<div class="order">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-6 col-md-5">
                    <div class="previewMak">
                        <div class="previewMak__title">
                            Отображение лицевой стороны листа
                        </div>
                        <div class="previewMak__img">
                            <a href="{{ $thumb['front'] }}" data-fancybox="gallery">
                                <img src="{{ $thumb['front'] }}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-5 offset-md-1">
                    <div class="previewMak">
                        <div class="previewMak__title">
                            Отображение оборотной стороны листа
                        </div>
                        <div class="previewMak__img">
                            <a href="{{ $thumb['back'] }}" data-fancybox="gallery">
                                <img src="{{ $thumb['back'] }}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex align-items-center">
            <div class="previewControl">
                @if($count > 1)
                <a href="#" class="previewControl__btn btn btn-outline-primary" data-toggle="modal" data-target="#modal-fullscreen">Проверить постранично</a>
                @endif
                <label class="previewControl__chck custom-control custom-checkbox">
                    <input type="checkbox" class="validate validate-ch custom-control-input" required value="Y" name="accepted">
                    <span class="custom-control-label">Утвердить макет заявки</span>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fullscreen" id="modal-fullscreen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="h2 text-center mb-5">Режим предосмотра макета</h2>
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="prevModal">
                                <div class="prevModal__img">
                                    <a id="frontImageWrap" href="{{ $thumb['front'] }}" data-fancybox="gallerymod">
                                        <img id="frontImage" src="{{ $thumb['front'] }}">
                                    </a>
                                </div>
                                <div class="prevModal__img">
                                    <a id="backImageWrap" href="{{ $thumb['c_back'] }}" data-fancybox="gallerymod">
                                        <img id="backImage" src="{{ $thumb['c_back'] }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="prevModalText">
                                <input type="hidden" id="currPage" value="1">
                                <input type="hidden" id="maxPage" value="{{ $count }}">
                                <div class="prevModalText__num">
                                    <span id="currentMail">1</span><i> / </i><span>{{ $count }}</span>
                                </div>
                                <div class="prevModalText__nav d-flex justify-content-between">
                                    <a href="#" onclick="prevPage()" class="prevModalText__back btn btn-primary">Предыдущая</a>
                                    <a href="#"  onclick="nextPage()" class="prevModalText__next btn btn-primary">Следующая</a>
                                </div>
                                <div class="prevModalText__per">
                                    Показать: <label><input type="radio" name="pages" value="1" checked><span>1 стр</span></label>
                                    @if($count>19)
                                    <label><input type="radio" name="pages" value="10"><span>по 10 стр</span></label>
                                    @endif
                                    @if($count>199)
                                    <label><input type="radio" name="pages" value="100"><span>по 100 стр</span></label>
                                    @endif
                                </div>
                                <div class="prevModalText__in">
                                    <div class="calcIn">
                                        <div class="calcIn__inp">
                                            <label for="calculate">Показать страницу №</label>
                                            <input data-mask="0#" type="text" value="" name="count" id="pnum" placeholder="{{ $count }}" min="1">
                                        </div>
                                        <a href="#" onclick="setPage()"  class="calcIn__btn">
                                            Показать
                                        </a>
                                    </div>
                                </div>
                                <a href="#" class="previewControl__btn btn btn-outline-primary" data-dismiss="modal" >Выйти из режима предосмотра</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="orderBottom">
    <div class="row">
        <div class="col-md-2 order-2 order-md-1 mb-md-0 mb-4">
            <a href="#" onclick="changePage(1, event)" class="orderBottom__back btn btn-primary">НАЗАД</a>
        </div>
        <div class="col-md-8 order-1 order-md-2 mb-md-0 mb-4">
            @include('steps.steper')
        </div>
        <div class="col-md-2 order-3 order-md-3">
            <a href="#" onclick="changePage(3, event)" id="nextPage" style="display: none" class="orderBottom__next btn btn-primary">ДАЛЕЕ</a>
        </div>
    </div>
</div>
<div class="loader" style="display:none;">
    <div class="loader__img">
        <img src="../img/svg/Loader.svg">
    </div>
    <div class="loader__animate">
        <div class="loader-bubble loader-bubble-white m-2"></div>
    </div>
    <div class="loader__text">
        Ваш макет почти готов!
    </div>
</div>
