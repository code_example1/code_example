<div class="orderStepper">
    <div class="orderStepperItem @if(!$draft->step || $draft->step >= 1) active @endif @if($draft->step > 1) complete @endif">
        <div class="orderStepperItem__num">1</div>
        <div class="orderStepperItem__text">
            Формирование заявки
        </div>
    </div>
    <div class="orderStepperItem @if($draft->step >= 2) active @endif @if($draft->step > 2) complete @endif">
        <div class="orderStepperItem__num">2</div>
        <div class="orderStepperItem__text">
            Утверждение макета
        </div>
    </div>
    <div class="orderStepperItem @if($draft->step >= 3) active @endif @if($draft->step > 3) complete @endif">
        <div class="orderStepperItem__num">3</div>
        <div class="orderStepperItem__text">
            Размещение заявки
        </div>
    </div>
</div>
