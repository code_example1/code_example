<?php

namespace App;

use App\Imports\ListImport;
use App\Notifications\CreateOrder;
use hackerESQ\Settings\Facades\Settings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use mikehaertl\wkhtmlto\Pdf;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Exports\AdrRecipientExport;

class Draft extends Model
{
    protected $fillable = ['name', 'type', 'accepted', 'step', 'pages', 'user_id', 'file_input', 'file_list', 'finished', 'order_id', 'otprav', 'fiz_adr', 'ded_sender'];
    protected $appends = ['can_order', 'otprav_list'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function storeFile($file, $field, $type = 'input', $ext = false) {
        if($this->$field) {
            if($field == 'file_input') {
                $this->removeInput();
            } else {
                Storage::delete($this->$field);
            }
        }

        if(File::exists($this->getPath() . '/thumbs')) {
            File::deleteDirectory($this->getPath() . '/thumbs');
        }
        if(!$ext) {
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = $type.'.'.$extension;

            $file->storeAs(
                'drafts/'.$this->id, $fileNameToStore
            );
        } else {
            $extension = $ext;
            $fileNameToStore = $type.'.'.$ext;
            File::copy($file, $this->getPath().'/'.$fileNameToStore);

        }



        $this->$field = 'drafts/'.$this->id.'/'.$fileNameToStore;




        if($field == 'file_input') {
            Storage::delete($this->file_list);

            $this->file_list = null;
            $this->pages = null;
            if($extension !== 'pdf') {
                $this->convertInput(storage_path('app/drafts/'.$this->id.'/'.$fileNameToStore));
            }
            $countInput = countPages($this->getPath().'/input.pdf');
            if($countInput > 1000) {
                throw new \ErrorException("Количество страниц в пределах одной заявки не может превышать 1000 (вы загрузили - $countInput ст.)");
            }
            $this->checkSizes(storage_path('app/drafts/'.$this->id.'/'.$fileNameToStore), $countInput);
        }

        if($field == 'file_list') {
            $countInput = countPages($this->getPath().'/input.pdf');
            $count = $this->getList(true);
            if($countInput > 3 && $count != $countInput) {
                throw new \ErrorException("Количество строк в базе рассылки не соответствует количеству страниц письма - $countInput ст.");
            } else if($count > 3000) {
                throw new \ErrorException("Количество страниц в пределах одной заявки не может превышать 3000 (вы загрузили - $count ст.)");
            } else {
                $this->checkPages();
            }

            $this->pages = count($this->getList());
        }

        return $this->save();
    }

    public function checkPages() {
        $countInput = countPages($this->getPath().'/input.pdf');
        $list = $this->getList();
        foreach ($list as $item) {
            if ($item['pages'] > $countInput) {
                throw new \ErrorException("Количество страниц в отдельном письме не может превышать количество страниц в исходном файле.");
            }
        }

    }


    public function removeInput(){
        if(File::exists($this->getPath() . '/input.pdf')) {
            File::delete($this->getPath() . '/input.pdf');
        }
        if(File::exists($this->getPath() . '/input.docx')) {
            File::delete($this->getPath() . '/input.docx');
        }
        if(File::exists($this->getPath() . '/input.rtf')) {
            File::delete($this->getPath() . '/input.rtf');
        }
    }


    public function convertInput($file) {
        shell_exec('export HOME=/tmp && libreoffice --headless --convert-to pdf '.$file.' --outdir '.$this->getPath());
    }

    public function checkSizes($file, $count, $res = false) {
        $count = intval($count);
        $pdfinfo = shell_exec('pdfinfo -f 1 -l '.$count.' '.$this->getPath() . '/input.pdf');
        preg_match_all('/Page.*size:\s+([0-9]{0,5}\.?[0-9]{0,3}) x ([0-9]{0,5}\.?[0-9]{0,3})[^A4][^\n]+(A4)/', $pdfinfo, $matches);
        preg_match_all('/Page.*rot:\s+([0-9]{0,5}\.?[0-9]{0,3})/', $pdfinfo, $rot);
        if(count($matches[0]) == $count) {
            for($i=0;$i<$count;$i++) {
                if($rot[1][$i] == '0') {
                    if($matches[1][$i] > $matches[2][$i]) {
                        throw new \ErrorException("Допустима только книжная раскладка файла");
                    }
                } elseif($rot[1][$i] == '90') {
                    if($matches[1][$i] < $matches[2][$i]) {
                        throw new \ErrorException("Допустима только книжная раскладка файла");
                    }
                } else {
                    throw new \ErrorException("Допустима только книжная раскладка файла");
                }
            }
        } else {
            if($res) {
                throw new \ErrorException("Допустимый размер страницы – 210 на 297 миллиметров (A4)");
            } else {
                $this->resizeInput($file, $count);
                $this->checkSizes($file, $count, true);
            }
        }
    }

    public function resizeInput() {
        shell_exec('pdfjam '.$this->getPath().'/input.pdf --a4paper --outfile '.$this->getPath().'/input.pdf');
    }

    public function getList($returnCount = false)
    {
        $pathList = $this->file_list;
        if (Storage::exists($pathList)) {
            //$items = Excel::import(new ListImport, $pathList);

            $import = new ListImport($this->user->fiz);
            $import->import($pathList);
            $items = $import->data;
            

            $return = array_filter($items, function($v) {
                $allEmpty = false;
                foreach( $v as $key => $val ) {
                    if( isset( $v[$key] ) ) {
                        $allEmpty = true;
                        break;
                    }
                }

                return $allEmpty;
            });
            if($returnCount) {
                $count = 0;
                foreach ($return as $rt) {
                    $count += $rt['pages'];
                }
                return $count;
            }
            return $return;
        } else {
            if($returnCount) {
                return 0;
            }
            return [];
        }

    }


    public function getPath() {
        $path = storage_path('app/drafts/'.$this->id);
        if(!File::exists($path)) {
            $this->createPath();
        }
        return $path;
    }

    public function createPath() {
        $path =  storage_path('app/drafts/'.$this->id);
        File::makeDirectory($path, $mode = 0777, true, true);
    }

    public function createBacks($list) {
//        File::makeDirectory($this->getPath().'/list', $mode = 0777, true, true);
//        File::makeDirectory($this->getPath().'/list-pdf', $mode = 0777, true, true);
        $pages = [];
        $client = 99999;
        $pag = 99;
        foreach ($list as $key=>$item) {
            $dmcodes = [];
            for ($i=1;$i<=$item['pages'];$i++) {
                $clnt = str_pad($client, 5, '0', STR_PAD_LEFT);
                $pgs = str_pad($pag, 2, '0', STR_PAD_LEFT);
                $oper = $i==1?'00001':'00000';
                $generator = new \App\Datamatrix\Datamatrix();
                $dmsvg = $generator->render_svg('dmtxs', $clnt.$pgs.$oper, []);
                $dmcodes[$i] = $dmsvg;


                $pag--;
                if ($pag == 0) {
                    $pag = 99;
                }
            }
            $client--;
            if ($client == 0) {
                $client = 99999;
            }
            $data = array(
                'last_name' => $item['last_name'],
                'first_name' => $item['first_name'],
                'mid_name' => $item['middle_name'],
                'address' => implode(' ', array_filter([$item['street'], $item['house'], $item['flat']])),
                'city' => $item['city'],
                'region' => $item['region'],
                'post' => $item['post_index'],
                'org_adr' =>  $this->user->fiz ? $this->fiz_adr : $this->user->org->{$this->otprav.'_address'},
                'pages'  => $item['pages'],
                'dm'    => $dmcodes,
                'num'   => $key+1
            );

            if($this->ded_sender == 1) {
                $data['org_name'] = Settings::get('ded_name');
            } else {
                $data['org_name'] = $this->user->fiz ? $this->user->full_name : $this->user->org->name;
            }

            $pages[] = $data;
        }

        $pdf = new Pdf(array(
            'no-outline',
            'margin-top'    => 0,
            'margin-right'  => 0,
            'margin-bottom' => 0,
            'margin-left'   => 0,
            'disable-smart-shrinking',
        ));
        $pdf->addPage(view('pdf.back', compact('pages'))->render());
        if (!$pdf->saveAs($this->getPath().'/backs.pdf')) {
            $error = $pdf->getError();
            throw new \ErrorException($pdf->getError());
        }

//        $tmpPath = $this->getPath().'/backs.docx';
//        $templateProcessor = new TemplateProcessor(storage_path('app/docs/templates/back2.docx'));
//        $templateProcessor->cloneBlock('table', 0, true, false, $pages);
//        $templateProcessor->saveAs($tmpPath);
//
//        shell_exec('libreoffice --headless --convert-to pdf '.$this->getPath().'/backs.docx --outdir '.$this->getPath());

    }

    public function createFront($list) {
        $pages = countPages($this->getPath().'/input.pdf');
            if($pages <= 3) {
                File::makeDirectory($this->getPath().'/front', $mode = 0777, true, true);
                $path = $this->getPath();

                for ($i=1; $i<=$this->pages; $i++){
                    shell_exec("cp $path/input.pdf $path/front/front_$i.pdf");
                }
                shell_exec('qpdf --empty --pages '.$this->getPath().'/front/*.pdf -- '.$this->getPath().'/fronts.pdf');
            } else {
                $path = $this->getPath();
                shell_exec("cp $path/input.pdf $path/fronts.pdf");
            }
        
    }

    public function mergePdf() {
        shell_exec('qpdf --collate '.$this->getPath().'/fronts.pdf --pages '.$this->getPath().'/fronts.pdf '.$this->getPath().'/backs.pdf -- '.$this->getPath().'/order.pdf');
    }

    public function copyToOrder() {
        $path = $this->order->path();
        if(File::exists($this->getPath() . '/front')) {
            File::deleteDirectory($this->getPath() . '/front');
        }
//        if(File::exists($this->getPath() . '/list')) {
//            File::deleteDirectory($this->getPath() . '/list');
//        }
//        if(File::exists($this->getPath() . '/list-pdf')) {
//            File::deleteDirectory($this->getPath() . '/list-pdf');
//        }
        if(File::exists($this->getPath() . '/thumbs')) {
            File::deleteDirectory($this->getPath() . '/thumbs');
        }
        File::copyDirectory($this->getPath(), $path);

    }

    public function createPdf() {
        $list = $this->getList();
        $this->createBacks($list);
        $this->createFront($list);
        $this->mergePdf();
        $this->copyToOrder();
    }



    public function createOrder() {
        if($this->type == 2) {
            $tarif = Settings::get('ny_tarif');
        } else {
            $tarif = Settings::get('tarif');
        }
        if(!$this->finished) {
            try {
                $this->finished = true;
                $this->save();
                $user = Auth::user();

                $order = Order::create([
                    'name' => $this->name,
                    'count' => $this->pages,
                    'price' => $this->pages * $tarif,
                    'status' => $this->user->fiz ? 'Ожидает оплату' : 'Очередь на печать',
                    'draft_type' => $this->type,
                    'user_id' => $this->user->id,
                    'org_id' => $this->user->org->id,
                    'adr' => $this->user->fiz ? $this->fiz_adr : $this->user->org->{$this->otprav.'_address'},
                    'pages' => $this->getList(true),
                    'ded_sender' => $this->ded_sender
                ]);

                $this->order_id = $order->id;
                $this->copyThumbs();

                $managers = User::role(['manager','admin'])->get()->all();
                foreach ($managers as $manager) {
                    $manager->notify(new CreateOrder($order));
                }


                $user->removeTrigger(['type6', 'type1', 'type2']);
                if(!$user->fiz) {
                    $user->makeTrigger(['type6']);
                }

                $this->save();

                return $order;
            } catch (\Exception $e) {
                $this->finished = false;
                $this->step = 1;
                $this->save();
                abort(403);
            }

        } else {
            abort(403);
        }
    }

    public function copyThumbs() {
        $path = $this->order->path();
        $images = $this->getImages();
        if(File::exists($this->getPath().'/thumbs/item1_front.jpg')) {
            File::copy($this->getPath().'/thumbs/item1_front.jpg', $path.'/front.jpg');
        }
        if(File::exists($this->getPath().'/thumbs/item1_back.jpg')) {
            File::copy($this->getPath().'/thumbs/item1_back.jpg', $path.'/back.jpg');
        }
    }

    public function clearThumbs() {
        $path = $this->getPath().'/thumbs';
        if(File::exists($path)) {
            File::deleteDirectory($path);
        }
    }

    public function getImages($num = 1) {
        $num  = intval($num);
        $path = $this->getPath().'/thumbs';

        if(!File::exists($this->getPath().'/thumbs/item'.$num.'_c_back.jpg') || !File::exists($this->getPath().'/thumbs/item'.$num.'_back.jpg') || !File::exists($this->getPath().'/thumbs/item'.$num.'_front.jpg')) {


        if(!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }


        //get back image
        $list = $this->getList();
        $pages = (int) countPages($this->getPath().'/input.pdf');
//        $item = $list[$num-1];
//        $tmpPath = $this->getPath().'/thumbs/item'.$num.'.docx';
//        $templateProcessor = new TemplateProcessor(storage_path('app/docs/templates/back.docx'));
//        $templateProcessor->setValues(
//            array(
//                'last_name' => $item['last_name'],
//                'first_name' => $item['first_name'],
//                'mid_name' => $item['middle_name'],
//                'address' => implode(' ', array_filter([$item['street'], $item['house'], $item['flat']])),
//                'city'   => $item['city'],
//                'region' => $item['region'],
//                'post'    => $item['post_index'],
//                'org_name' => $this->user->fiz ? $this->user->full_name : $this->user->org->name,
//                'org_adr' =>  $this->user->fiz ? $this->fiz_adr : $this->user->org->{$this->otprav.'_address'}
//            )
//        );
//        $templateProcessor->saveAs($tmpPath);
//        shell_exec('libreoffice --headless --convert-to pdf '.$this->getPath().'/thumbs/item'.$num.'.docx --outdir '.$this->getPath().'/thumbs');
//        File::delete($tmpPath);
//
//        shell_exec('convert -crop 374x182+376+139 -density 100 -background white -alpha flatten '.$this->getPath().'/thumbs/item'.$num.'.pdf '.$this->getPath().'/thumbs/item'.$num.'_c_back.jpg');
//        shell_exec('convert -density 50 -background white -alpha flatten '.$this->getPath().'/thumbs/item'.$num.'.pdf '.$this->getPath().'/thumbs/item'.$num.'_back.jpg');

        shell_exec('convert -density 85 -background white -alpha flatten '.$this->getPath().'/backs.pdf['.($num-1).'] '.$this->getPath().'/thumbs/item'.$num.'_back.jpg');
        if($pages == 1) {
            shell_exec('convert -density 85 -background white -alpha flatten '.$this->getPath().'/input.pdf '.$this->getPath().'/thumbs/item'.$num.'_front.jpg');
        }
        elseif($pages <= 3 && $num > $pages) {
            $page = $num % $pages ? $num % $pages : $pages;
            
            shell_exec('convert -density 85 -background white -alpha flatten '.$this->getPath().'/input.pdf['.($page-1).'] '.$this->getPath().'/thumbs/item'.$num.'_front.jpg');
        }
        else {
            shell_exec('convert -density 85 -background white -alpha flatten '.$this->getPath().'/input.pdf['.($num-1).'] '.$this->getPath().'/thumbs/item'.$num.'_front.jpg');
        }
        //File::delete($this->getPath().'/thumbs/item'.$num.'.pdf');
        }

        return [
            'front' => route('thumb', [$this, '/thumbs/item'.$num.'_front.jpg']),
            'back' => route('thumb', [$this, '/thumbs/item'.$num.'_back.jpg']),
            'c_back' => route('thumb', [$this, '/thumbs/item'.$num.'_back.jpg']),
        ];

    }

    public function accepted ($accepted = true) {
        $this->accepted = $accepted;
        $this->save();
    }

    public function type ($type = 1) {
        $this->type = $type;
        $this->save();
    }

    public function getCanOrderAttribute () {
        $can = true;
        if($this->user->fiz) {
            return $can;
        }
        $org = $this->user->org;
        $tarif = Settings::get('tarif');
        $price = intval($this->pages) * $tarif;

        if($price > $org->balance || !$this->pages) {
            $can = false;
        }
        return $can;
    }

    public function getOtpravListAttribute () {

        $org = $this->user->org;
        $list = [
            'ur' => $org->ur_address,
            'fact' => $org->fact_address,
            'post' => $org->post_address
        ];

        return $list;
    }

    public function getNYGreetingsTemplates () {
        $result = array();
        $catalogTemp = storage_path('app/docs/templates/ny_greetings_templates/');

        $allTemplates = scandir($catalogTemp);
        $templatesFiles = array_diff($allTemplates, array('.', '..', 'thumbs'));

        if(!empty($templatesFiles)) {
            foreach ($templatesFiles as $tempFile) {
                $partsName = explode('.', $tempFile);
                $nameWithoutExt = $partsName[0];

                $result[] = array(
                    'name' => $partsName[0],
                    'url' => route('ny-template-get', $tempFile),
                    'thumb' => route('ny-thumb-templates', $nameWithoutExt . '.jpg'),
                );
            }
        }

        return $result;
    }

    public function saveAdrRecipient($dataAdrRecipient) {
        $filePath = $this->getPath() .'/table.xlsx';

        if(File::exists($filePath)) {
            Storage::delete($filePath);
        }

        Excel::store(new AdrRecipientExport($dataAdrRecipient), 'drafts/'.$this->id.'/table.xlsx', 'local');

        $this->file_list = 'drafts/'.$this->id.'/table.xlsx';
        $this->pages = count($this->getList());
        $this->save();

        //$this->storeFile($filePath, 'file_list', 'table', 'xlsx');
    }

    public function checkQuantityPagesInTamplate() {
        $result = true;

        $pages = countPages($this->getPath().'/input.pdf');

        if($pages < 1) {
            $result = "Минимальное количество страниц для письма - 1";
        } elseif($pages > 3) {
            $result = "Максимальное количество страниц для письма - 3";
        }

        return $result;
    }
}
