<?php

namespace App\Http\Controllers;

use App\Order;
use App\Org;
use App\Rules\Digits;
use App\Rules\Lowcase;
use App\Rules\Symbol;
use App\Rules\Uppercase;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use hackerESQ\Settings\Facades\Settings;

class UserController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->confirm && $request->confirm == "Y") {
            return redirect()->route('user-dash')->with(['targetLink' => 'Y']);
        }
        $user = Auth::user();
        $goal = session()->get( 'targetLink' ) == 'Y' ? true : false;
        $ny_tarif = Settings::get('ny_tarif');
        if($user->fiz) {
            return view('user.dashboard-fiz', compact('user', 'goal', 'ny_tarif'));
        } else {
            return view('user.dashboard', compact('user', 'goal', 'ny_tarif'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if($user->org->moderate) {
            return view('user.add', compact('user'));
        } else {
            return redirect(route('user-show'))->with('error', 'Организация еще не подтверждена, добавление пользователей возможно только после подтверждения.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if($user->org->moderate) {
            $validatedData = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'lastname' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'policy' => ['accepted'],
            ]);

            $fields = $request->all();

            $user->addUser($fields);

            return redirect(route('user-show'))->with('success', 'Пользователь добавлен.');
        } else {
            return redirect(route('user-show'))->with('error', 'Организация еще не подтверждена, добавление пользователей возможно только после подтверждения.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('user.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('user.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {

        $user = Auth::user();
        $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed', new Uppercase, new Lowcase, new Digits],
        ]);
        $fields = $request->all();

        if($fields['password'] === null) {
            unset($fields['password']);
        } elseif(isset($fields['password'])) {
            $fields['password'] = Hash::make($fields['password']);
        }
        $user->fill($fields);
        $changes = $user->getDirty();
        $user->save();

        return redirect(route('user-show'));

    }

    public function actions(Request $request, $action, $element)
    {
        switch ($action) {
            case 'rename':
                $validatedData = $request->validate([
                    'name' => ['required', 'string', 'max:255']
                ]);

                $user = Auth::user();
                $order = $user->org->orders()->where('id', $element)->firstOrFail();
                $order->name = $request->name;
                $order->timestamps = false;
                $order->save();
                return ['update_mess'=>'Заявка переименована'];
                break;
        }
    }


    public function loadModal($action, $element)
    {
        $item = null;

        switch ($action) {
            case 'rename':
                $user = Auth::user();
                $item = $user->org->orders()->where('id', $element)->firstOrFail();
                break;
            case 'download':
                $user = Auth::user();
                $order = $user->org->orders()->where('id', $element)->firstOrFail();
                $item = $order->getDocuments();
                break;
            case 'remove-bill':
                $user = Auth::user();
                $item = $user->org->bills()->where('id', $element)->firstOrFail();
                break;
        }
        return view('user.modal.'.$action, compact('item'));
    }

    public function loadOrder(Order $order)
    {
        $user = Auth::user();
        if (!$user->hasRole(['manager', 'admin'])) {
            if ($user->org_id !== $order->org_id) {
                abort('404');
            }
        }

        return view('user.modal.order', compact('order'));
    }
}
