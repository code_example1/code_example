<?php

namespace App;

use App\Notifications\NewUser;
use App\Notifications\EmailVerification;
use App\Notifications\UserPass;
use hackerESQ\Settings\Facades\Settings;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'lastname', 'email', 'password', 'phone', 'user_id', 'org_id', 'unsubs'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'fiz'
    ];

    public function org() {
        return $this->belongsTo(Org::class);
    }

    public function orders () {
        return $this->hasMany(Order::class);
    }

    public function bills () {
        return $this->hasMany(Bill::class);
    }

    public function draft () {
        return $this->hasMany(Draft::class);
    }

    public function triggers () {
        return $this->hasMany(Trigger::class);
    }

    public function scopeFiz($query)
    {
        return $query->where('org_id', Settings::get('fiz'));
    }

    public function getNameAttribute($value)
    {
        return mb_ucfirst($value);
    }

    public function getLastnameAttribute($value)
    {
        return mb_ucfirst($value);
    }

    public function getSurnameAttribute($value)
    {
        return mb_ucfirst($value);
    }

    public function getFullNameAttribute()
    {
        return mb_ucfirst($this->name) . ' ' . mb_ucfirst($this->surname) . ' ' . mb_ucfirst($this->lastname);
    }

    public function resetPass()
    {
        $newPass = pass_gen();
        $this->password = Hash::make($newPass);
        $this->save();
        $this->notify(new UserPass($newPass));
    }

    public function addUser($fields)
    {
        $newUser = new User;
        $newUser->fill($fields);
        $password = pass_gen();
        $newUser->password = Hash::make($password);
        $newUser->org_id = $this->org_id;
        $newUser->save();
        $newUser->notify(new NewUser($password));
    }

    public function getDraft($type = 1)
    {
        return Draft::firstOrCreate(['user_id'=>$this->id, 'finished' => false, 'type' => $type]);
    }


    public function removeTrigger($types) {
        if (!is_array($types)) {
            $types = array($types);
        }

        foreach ($types as $type) {
            Trigger::where([
                'type' => $type,
                'org_id' => $this->org_id
            ])->delete();
        }
    }

    public function makeTrigger($types) {
        if (!is_array($types)) {
            $types = array($types);
        }
        foreach ($types as $type) {
            if(!Trigger::where(['type'=>$type, 'org_id'=>$this->org_id])->exists()) {
                Trigger::create([
                    'type' => $type,
                    'org_id' => $this->org_id,
                    'user_id' => $this->id
                ]);
            }
        }
    }

    public function byCard() {
        $lastBill = $this->org->bills->last();
        $lastPay = $this->org->pays->last();
        if($lastPay) {
            if($lastBill) {
                if($lastPay->created_at->gt($lastBill->created_at)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }

        }
        return false;
    }

    public function getFizAttribute() {
        return $this->org->id == Settings::get('fiz') ? true : false;
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new EmailVerification());
    }
}
